﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using DotAmf.Serialization;
using System.Drawing;
using System.IO;

namespace windowsdriver.record
{
    internal interface IVisualAction
    {
        void AddImage(VisualRecorder visualRecorder, double[] channelBound, bool v);
        void AddImage(VisualRecorder visualRecorder, string url, double[] channelBound, bool v);
        void AddImage(Bitmap bytes);
        void AddImage(string type);
        void AddRecord(string path);
        void setData(string v);
        void setDuration(long duration);
        void setElement(VisualElement visualElement);
        void setError(int error);
        int setIndex(int frameIndex);
        void setValue(string v);
        void UpdateElementPosition(string hpos, string hposValue, string vpos, string vposValue);
        void WriteAction(DataContractAmfSerializer amfSerializer, BufferedStream visualStream);
    }
}
