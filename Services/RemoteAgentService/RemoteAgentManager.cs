﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Net;
using System.Threading;

namespace windowsdriver.Services.RemoteAgentService
{
    /// <summary>
    /// Manages the operations and state of a remote agent.
    /// This class provides functionalities to interact with the agent's data and status.
    /// </summary>
    public class RemoteAgentManager : IDisposable
    {
        public RemoteAgentData remoteAgentData;
        private WebServer webServer;

        /// <summary>
        /// Constructor for RemoteAgentManager.
        /// Initializes the RemoteAgentData instance.
        /// </summary>
        public RemoteAgentManager(WebServer webServer)
        {
            remoteAgentData = new RemoteAgentData();
            this.webServer = webServer;
        }

        /// <summary>
        /// Locks the remote agent.
        /// </summary>
        /// <param name="lockedBy">Identifier of the entity locking the agent.</param>
        public void LockAgent(string lockedBy)
        {
            remoteAgentData.Locked = true;
            remoteAgentData.LockedBy = lockedBy;
            UpdateLastPingTime();
        }

        /// <summary>
        /// Unlocks the remote agent.
        /// </summary>
        public void UnlockAgent()
        {
            webServer.ShutdownUnlock();
            Dispose();
        }

        /// <summary>
        /// returns true if the remote agent is locked, false otherwise.
        /// </summary>
        public bool IsLocked()
        {
            return remoteAgentData.Locked;
        }

        /// <summary>
        /// Returns the identifier of the entity that has locked the remote agent.
        /// </summary>
        /// <returns>string identifier lock</returns>
        public string GetLockedBy()
        {
            return remoteAgentData.LockedBy;
        }

        /// <summary>
        /// Updates the last ping time of the remote agent.
        /// </summary>
        public void UpdateLastPingTime()
        {
            remoteAgentData.LastPingTime = System.DateTime.Now;
        }

        /// <summary>
        /// Initializes the timeout timer for the remote agent.
        /// <summary>
        public void InitTimer()
        {
            if (remoteAgentData.TimeoutTimer == null)
            {
                remoteAgentData.TimeoutTimer = new Timer(CheckTimeout, null, TimeSpan.Zero, TimeSpan.FromSeconds(remoteAgentData.TimerInterval));
            }
        }

        /// <summary>
        /// Checks if the remote agent has timed out.
        /// </summary>
        public void CheckTimeout(object state)
        {
            if (IsLocked() && IsTimeout())
            {
                UnlockAgent();
            }
        }

        /// <summary>
        /// Returns true if the remote agent has timed out, false otherwise.
        /// <returns>bool timeout</returns>
        /// </summary>
        public bool IsTimeout()
        {
            return ((DateTime.Now - remoteAgentData.LastPingTime).TotalSeconds > remoteAgentData.timeout);
        }

        /// <summary>
        /// Returns the IP address of the request.
        /// </summary>
        /// <param name="listener"></param>
        /// <returns>Ip address of request</returns>
        public string GetRequestAddressIp(HttpListenerContext listener)
        {
            var remoteAddressIp = "Unknown";
            if (listener.Request.RemoteEndPoint != null && listener.Request.RemoteEndPoint.Address != null)
            {
                remoteAddressIp = listener.Request.RemoteEndPoint.Address.ToString();
            }

            return remoteAddressIp;
        }

        /// <summary>
        /// Disposes the timeout timer.
        /// </summary>
        public void Dispose()
        {
            remoteAgentData.Locked = false;
            remoteAgentData.LockedBy = string.Empty;
            remoteAgentData.TimeoutTimer?.Dispose();
        }
    }
}
