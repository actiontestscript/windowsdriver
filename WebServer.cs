﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using WatsonWebsocket;
using windowsdriver;
using windowsdriver.items;
using windowsdriver.Services.CommandProcessing;
using windowsdriver.utils;
using windowsdriver.utils.JsonUtils;
using windowsdriver.Utils.Logger;
using windowsdriver.w3c;
using windowsdriver.w3c.ats;
using windowsdriver.w3c.ats.recorder;
using windowsdriver.w3c.session;

public class WebServer
{
    [DllImport("shell32.dll")]
    internal static extern bool IsUserAnAdmin();

    private const string ATS_USER_AGENT = "AtsDesktopDriver";
    private const int WEBSOCKET_START_PORT = 9800;

    private bool isRunning = true;
    private bool keyDown = false;

    private HttpListener listener;

    private readonly ActionKeyboard keyboard = new ActionKeyboard();
    private readonly VisualRecorder recorder = new VisualRecorder();

    private readonly Capabilities capabilities;
    private readonly DesktopManager desktop;
    private readonly CommandResult commandResult;

    //private readonly WebSocketServer webSocket;

    public WebServer(int port, DesktopManager desktop, Capabilities capabilities, CommandResult commandResult)
    {
        if (!HttpListener.IsSupported)
            throw new NotSupportedException(
                "Needs Windows XP SP2, Server 2003 or later.");

        this.desktop = desktop;
        this.capabilities = capabilities;
        this.commandResult = commandResult;

        string driverVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        //-----------------------------------------------------------------------------
        // init web settings
        //-----------------------------------------------------------------------------

        //pour eviter l'elevation de droit
        //        netsh http add urlacl url = http://localhost:9700/ user=YOUR_USERNAME
        //        netsh http add urlacl url = http://127.0.0.1:9700/ user=YOUR_USERNAME
        //        netsh http add urlacl url = http://192.168.0.156:9700/ user=YOUR_USERNAME
        // pour delete
        //        netsh http delete urlacl url=http://192.168.0.156:9700/
        //pour voir les url
        //        netsh http show urlacl || netsh http show urlacl |findstr :9700

        //---------------------------------------------------------------------------------------------------
        // port
        //---------------------------------------------------------------------------------------------------

        while (Network.IsPortInUse(port))
        {
            port++;
        }

        //---------------------------------------------------------------------------------------------------
        // prefixes
        //---------------------------------------------------------------------------------------------------
        
        var prefixesSet = new HashSet<string>() { "localhost", "127.0.0.1"};

        if (!commandResult.local)
        {
            if (IsUserAnAdmin())
            {
                var ipListLocal = Network.GetLocalIpAddresses();
                foreach (IPAddress addr in ipListLocal)
                {
                    prefixesSet.Add(addr.ToString());
                }
            }
            WsAgent.Local = false;
            Console.Clear();
        }

        //-----------------------------------------------------------------------------
        // Start web server
        //-----------------------------------------------------------------------------

        while (!StartServer(port, prefixesSet))
        {
            port++;
            Thread.Sleep(300);
        }

        Console.WriteLine("Starting ATS Windows Desktop Driver {0} on port {1}", driverVersion, port);

        if (prefixesSet.Count > 2)
        {
            Console.WriteLine("Allowed Ips: " + string.Join(",", commandResult.allowedIps));
        }
        else
        {
            Console.WriteLine("Only local connections are allowed.");
        }

        //---------------------------------------------------------------------------------------------------
        // finish start
        //---------------------------------------------------------------------------------------------------

        if (!string.IsNullOrEmpty(commandResult.globalIp))
        {
            commandResult.allowedIps.Add(commandResult.globalIp);
        }

        int webSocketPort = (int)IpUtilities.GetAvailablePort(WEBSOCKET_START_PORT, 65535);

       //int webSocketPort = WEBSOCKET_START_PORT;
        while(!CheckPortAvailable(webSocketPort))
        {
            webSocketPort++;
        }

        WatsonWsServer server = new WatsonWsServer("localhost", webSocketPort, false);
        server.ClientConnected += WsAgent.Connect;
        server.ClientDisconnected += WsAgent.Disconnect;
        server.MessageReceived += WsAgent.Update;
        server.Start();

        /*WebSocketServer webSocket = new WebSocketServer
        {
            //OnSend = OnSend,
            OnReceive = WsAgent.Update,
            OnConnected = WsAgent.Connect,
            OnDisconnect = WsAgent.Disconnect,
            TimeOut = new TimeSpan(0, 60, 0),
            Port = webSocketPort,
            ListenAddress = IPAddress.Any
        };
        webSocket.Start();*/

        Console.WriteLine("Websocket server started with port " + webSocketPort);

        WsAgent.SetData(driverVersion, port, webSocketPort);

        Console.WriteLine("WindowsDriver was started successfully.");

        // use WsAgent.UsedBy == null to check if Ats Agent is locked
    }

    public static class IpUtilities
    {
        private const ushort MIN_PORT = 1;
        private const ushort MAX_PORT = UInt16.MaxValue;
        public static int? GetAvailablePort(ushort lowerPort = MIN_PORT, ushort upperPort = MAX_PORT)
        {
            var ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            var usedPorts = Enumerable.Empty<int>()
                .Concat(ipProperties.GetActiveTcpConnections().Select(c => c.LocalEndPoint.Port))
                .Concat(ipProperties.GetActiveTcpListeners().Select(l => l.Port))
                .Concat(ipProperties.GetActiveUdpListeners().Select(l => l.Port))
                .ToHashSet();
            for (int port = lowerPort; port <= upperPort; port++)
            {
                if (!usedPorts.Contains(port)) return port;
            }
            return null;
        }
    }

    public static bool CheckPortAvailable(int port)
    {
        IPEndPoint ep = new IPEndPoint(IPAddress.Any, port);
        TcpListener tcp = new TcpListener(ep);
        bool result = false;

        try
        {
            tcp.Start();
            tcp.Stop();

            result = true;
        }
        catch (Exception e){
            result = false;
        }
        finally {
            //tcp.Server.Shutdown(SocketShutdown.Both);
        }

        return result;
    }

    //-----------------------------------------------------------------------------------------------------------

    private bool StartServer(int port, HashSet<string> prefixesSet)
    {
        listener = new HttpListener();

        try
        {
            foreach (var serverIp in prefixesSet)
            {
                listener.Prefixes.Add("http://" + serverIp + ":" + port + "/");
            }

            listener.IgnoreWriteExceptions = true;
            listener.Start();

            return true;
        }
        catch (HttpListenerException e)
        {
            if (e.ErrorCode == 5) //TODO check if this function is still necessary
            {
                //access denied
                /*Console.WriteLine("Access denied, please run the following command in an elevated command prompt:");
                foreach (var prefix in prefixesError)
                {
                    Console.WriteLine("netsh http add urlacl url={0} user=<USERNAME>", prefix);
                }
                Console.WriteLine("");
                Console.WriteLine("Or add arguments allowedIpServers=<IP_SERVER_ALLOWED>,<A_ANOTHER_IP_ALLOWED>");
                Console.WriteLine("");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                Environment.Exit(0);*/
                return true;
            }

            return false;
        }
        catch
        {
            return false;
        }
    }

    private bool SendResponse(HttpListenerContext context)
    {
        if (!WsAgent.Allowed(context.Request.Headers))
        {
            using (HttpListenerResponse resp = context.Response)
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes("agent is locked");

                resp.ContentType = "text/plain";
                resp.ContentLength64 = bytes.Length;
                resp.StatusCode = 423;

                resp.OutputStream.Write(bytes, 0, bytes.Length);
            }
            return true;
        }

        HttpListenerRequest request = context.Request;

        string method = request.HttpMethod;

        string urlPath = request.Url.AbsolutePath;
        if (urlPath.EndsWith("/"))
        {
            urlPath = urlPath.Substring(0, urlPath.Length - 1);
        }

        List<string> urlData = urlPath.Substring(1).Split('/').ToList();
        string firstUrlElem = urlData[0];
        DesktopRequest req = new DesktopRequest();


        if (string.IsNullOrEmpty(firstUrlElem))
        {
            using (HttpListenerResponse resp = context.Response)
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes("windows driver is started");

                resp.ContentType = "text/plain";
                resp.ContentLength64 = bytes.Length;
                resp.StatusCode = 201;

                resp.OutputStream.Write(bytes, 0, bytes.Length);
            }
            return true;
        }

        if (urlData.Count == 1)
        {
            if (firstUrlElem == "explorer")
            {
                new Explorer(context, method, capabilities, desktop).Send();
            }
            else if (firstUrlElem == "profile")
            {
                windowsdriver.w3c.ats.Profile profile = new windowsdriver.w3c.ats.Profile(context, method);
                profile.Send(profile.value);
            }
            else if (firstUrlElem == "session")
            {

                if (method.Equals("GET"))
                {
                    windowsdriver.w3c.ats.Session session =
                        new windowsdriver.w3c.ats.Session(context, method, capabilities, urlData);
                    session.Send(session.value);
                }
                else if (method.Equals("POST"))
                {
                    string jsonContent = JsonUtils.readJsonContent(context);
                    List<string> keyPath = new List<string> { "value", "ats" };
                    if (JsonUtils.CheckKeyPathExists(jsonContent, keyPath))
                    {
                        windowsdriver.w3c.ats.Session session =
                            new windowsdriver.w3c.ats.Session(context, method, capabilities, urlData,jsonContent);
                        session.Send(session.value);
                    }
                    else
                    {
                        string subType = string.Join("", urlData.Skip(2));
                        SessionW3c.SessionW3CType? type = SessionW3c.GetEnumSessionW3cType(subType);
                        SessionW3CFactory sessionW3cFactory = new SessionW3CFactory();
                        SessionW3c sessionW3c = sessionW3cFactory.Create(type, context);
                        if (sessionW3c != null)
                        {
                            sessionW3c.Execute();
                        }
                    }
                }
            }
            else if (firstUrlElem == "shutdown")
            {
                ShutdownUnlock();
                SendTextResponse(context, "shutdown windows driver ...");
                return false;
            }
            else if (firstUrlElem == "start")
            {
                SendTextResponse(context, AtsStartPage.GetHtmlPage(request.QueryString, capabilities));
            }
            else if (firstUrlElem == "status" )
            {
                Status status = new Status(context, WsAgent.UsedBy == null, capabilities);
                status.Send(status.value);
            }
            else if (firstUrlElem == "stop")
            {
                CachedElements.Instance.Clear(desktop.DesktopElement);
                SendTextResponse(context, "ok");
            }
            else if (firstUrlElem == "ping")
            {
                //remoteAgentManager.UpdateLastPingTime();
                var jsonResponse = new JsonResponse(context);
                jsonResponse.Send(jsonResponse.value);
                return true;
            }
        }
        else if (firstUrlElem.StartsWith("screencapture"))
        {
            SendBinaryResponse(context, VisualRecorder.ScreenCapturePng(desktop, urlData));
        }
        else if (firstUrlElem.StartsWith("window"))
        {
            windowsdriver.w3c.ats.WindowTitle window = new windowsdriver.w3c.ats.WindowTitle(context, method, capabilities, urlData, desktop);
            window.Send(window.value);
        }
        else if (urlData.Count > 2 && urlData[0].StartsWith("session") && urlData[2].StartsWith("ats"))
        {
            var subType = string.Join("", urlData.Skip(4));
            var toogleKeyDown = false;

            if (urlData[3].StartsWith("driver"))
            {
                windowsdriver.w3c.ats.driver.Driver.DriverType? type = windowsdriver.w3c.ats.driver.Driver.GetEnumDriverType(subType);
                windowsdriver.w3c.ats.driver.DriverFactory driverFactory = new windowsdriver.w3c.ats.driver.DriverFactory();
                windowsdriver.w3c.ats.driver.Driver driver = driverFactory.Create(type, context, capabilities, recorder, desktop);
                
                if (driver != null)
                {
                    if(windowsdriver.w3c.ats.driver.Driver.DriverType.Close == type)
                    {
                        return driver.Execute();
                    }

                    driver.Execute();

                }
                
            }
            if (urlData[3].StartsWith("keyboard"))
            {
                windowsdriver.w3c.ats.key.Key.KeyType? type = windowsdriver.w3c.ats.key.Key.getEnumKeyType(subType);
                windowsdriver.w3c.ats.key.KeyFactory keyFactory = new windowsdriver.w3c.ats.key.KeyFactory();
                windowsdriver.w3c.ats.key.Key key = keyFactory.Create(type, context, keyboard, keyDown);
                if (key != null)
                {
                    key.Execute();
                    if (type == windowsdriver.w3c.ats.key.Key.KeyType.Down)
                    {
                        toogleKeyDown = true;
                    }
                }
            }
            else if (urlData[3].StartsWith("element"))
            {
                windowsdriver.w3c.ats.element.Element.ElementType? type = windowsdriver.w3c.ats.element.Element.getEnumElementType(subType);
                windowsdriver.w3c.ats.element.ElementFactory elementFactory = new windowsdriver.w3c.ats.element.ElementFactory();
                windowsdriver.w3c.ats.element.Element element= elementFactory.Create(type, context, desktop);
                if (element != null)
                {
                    element.Execute();
                }
            }
            else if (urlData[3].StartsWith("mouse"))
            {
                windowsdriver.w3c.ats.mouse.Mouse.MouseType? type = windowsdriver.w3c.ats.mouse.Mouse.getEnumMouseType(subType);
                windowsdriver.w3c.ats.mouse.MouseFactory mouseFactory = new windowsdriver.w3c.ats.mouse.MouseFactory();
                windowsdriver.w3c.ats.mouse.Mouse mouse= mouseFactory.Create(type, context, desktop);
                if (mouse != null)
                {
                    mouse.Execute();
                }
            }
            else if (urlData[3].StartsWith("recorder"))
            {
                Recorder.RecordType? type = Recorder.getEnumRecordType(subType);
                RecorderFactory recorderFactory = new RecorderFactory();
                Recorder record = recorderFactory.Create(type,context, recorder);
                if(record != null)
                {
                    record.Execute();
                    if (type == Recorder.RecordType.Start)
                    {
                        toogleKeyDown = true;
                    }
                }
            }
            else if (urlData[3].StartsWith("window"))
            {
                windowsdriver.w3c.ats.windows.Window.WindowType? type = windowsdriver.w3c.ats.windows.Window.getEnumWindowType(subType);
                windowsdriver.w3c.ats.windows.WindowFactory windowFactory = new windowsdriver.w3c.ats.windows.WindowFactory();
                windowsdriver.w3c.ats.windows.Window window = windowFactory.Create(type, context, keyboard, desktop, recorder);
                if (window != null)
                {
                    window.Execute();
                }
            }
            keyDown = toogleKeyDown;
        }
        else if (urlData.Count > 1 && urlData[0].ToLower().StartsWith("session"))
        {
            string subType = string.Join("", urlData.Skip(2));
            SessionW3c.SessionW3CType? type = SessionW3c.GetEnumSessionW3cType(subType);
            SessionW3CFactory sessionW3cFactory = new SessionW3CFactory();
            SessionW3c sessionW3c = sessionW3cFactory.Create(type, context);
            if (sessionW3c != null)
            {
                sessionW3c.Execute();
            }
        }
        else
        {
            if (urlData.Count > 1)
            {
                try
                {
                    string postData = "";
                    using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
                    {
                        postData = reader.ReadToEnd();
                    }

                    if (int.TryParse(urlData[0], out int t0) && int.TryParse(urlData[1], out int t1))
                    {
                        string[] postDataTab = postData.Split('\n');
                        req.Init(method, t0, t1, postDataTab, keyboard, recorder, capabilities, desktop, keyDown);
                        keyDown = t1 == 2;
                    }
                    else
                    {
                        string[] cmdTab = new string[urlData.Count - 3];
                        urlData.CopyTo(3, cmdTab, 0, urlData.Count - 3);

                        string cmdSubType = cmdTab[1] + (cmdTab.Count() == 3 ? cmdTab[2] : string.Empty);

                        req.Init(method, cmdTab[0], cmdSubType, postData, keyboard, recorder, capabilities, desktop, keyDown);
                        keyDown = cmdSubType == "start";
                    }
                }
                catch (Exception ex)
                {
                    AtsLogger.WriteError("unknown error -> " + ex.Message);
                    req.Error(-3, context.Request.UserAgent.Equals(ATS_USER_AGENT), "unknown error -> " + ex.Message);
                }
                return req.Execute(context);
            }
        }
        return true;
    }

    private static void SendTextResponse(HttpListenerContext context, string textData)
    {
        byte[] sendBytes = System.Text.Encoding.UTF8.GetBytes(textData);
        int bytesLen = sendBytes.Length;

        using (HttpListenerResponse resp = context.Response)
        {
            resp.StatusCode = 200;
            resp.ContentType = "text/html";
            resp.ContentLength64 = bytesLen;
            resp.AddHeader("Cache-Control", "no-cache");

            using (Stream output = resp.OutputStream)
            {
                output.Write(sendBytes, 0, bytesLen);
            }
            resp.Close();
        }
    }

    private static void SendBinaryResponse(HttpListenerContext context, byte[] sendBytes)
    {
        int bytesLen = sendBytes.Length;

        using (HttpListenerResponse resp = context.Response)
        {
            resp.StatusCode = 200;
            resp.ContentType = "image/png";
            resp.ContentLength64 = bytesLen;
            resp.AddHeader("Cache-Control", "no-cache");
            resp.AddHeader("Content-Transfer-Encoding", "binary");

            using (Stream output = resp.OutputStream)
            {
                output.Write(sendBytes, 0, bytesLen);
            }
            resp.Close();
        }
    }

    public void Run()
    {
        while (isRunning)
        {
            _ = ThreadPool.QueueUserWorkItem(c =>
            {
                var ctx = c as HttpListenerContext;
                bool atsAgent = ctx.Request.UserAgent.Equals(ATS_USER_AGENT);
                try
                {
                    isRunning = SendResponse(ctx);
                }
                catch (Exception e)
                {

                    DesktopRequest req = new DesktopRequest(-99, atsAgent, "error -> " + e.Message + "\n" + e.StackTrace.ToString());
                    isRunning = req.Execute(ctx);
                }

            }, listener.GetContext());
        }
    }

    public void ShutdownUnlock()
    {
        capabilities.Dispose();
        desktop.Dispose();
        Console.Clear();
        recorder.Reset();
    }
}