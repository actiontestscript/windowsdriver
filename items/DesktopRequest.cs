﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using windowsdriver;
using windowsdriver.utils;
using windowsdriver.w3c;
using static WindowExecution;

class DesktopRequest : IDisposable
{
    private const int errorCode = -3;
    private const string errorMessage = "unkown command type";

    //TODO: to remove when w3c notation will be used 
    private enum CommandType
    {
        Driver = 0,
        Record = 1,
        Window = 2,
        Element = 3,
        Keyboard = 4,
        Mouse = 5
    };

    private AtsExecution execution;

    public DesktopRequest() { }


    public DesktopRequest(int errorCode, bool atsAgent, string message)
    {
        Error(errorCode, atsAgent, message);
    }

    public void Error(int errorCode, bool atsAgent, string message)
    {
        execution = new AtsExecution(errorCode, atsAgent, message);
    }

    public void Init(string method, string cmdType, string cmdSubType, JObject Node, ActionKeyboard keyboard, VisualRecorder recorder, Capabilities capabilities, DesktopManager desktop, bool keyDown = false)
    {
        switch (cmdType.ToLower())
        {
            case "window":
                execution = new WindowExecution( cmdSubType, Node, keyboard,recorder,desktop); 
                break;
            case "element":
                execution = execution = new ElementExecution(cmdSubType, Node, desktop);
                break;
//            case "recorder":
//                execution = new RecordExecution(method, cmdSubType, Node, recorder);
//                break;

        }
    }

    #region DesktopRequest
    public void Init(string method, string cmdType, string cmdSubType, string postData, ActionKeyboard keyboard, VisualRecorder recorder, Capabilities capabilities, DesktopManager desktop, bool keyDown = false)
    {
        switch (cmdType.ToLower())
        {
            case "driver":
                execution = new DriverExecution(cmdSubType, postData, capabilities, recorder, desktop);
                break;

//            case "recorder":
//                execution = new RecordExecution(method, cmdSubType, postData, recorder);
//                break;

            case "window":
                execution = new WindowExecution(cmdSubType, postData, keyboard, recorder, desktop);
                break; 

            case "element":
                execution = execution = new ElementExecution(cmdSubType, postData, desktop);
                break;

            case "keyboard":
                execution = new KeyboardExecution(cmdSubType, postData, keyboard, keyDown);
                break;

            case "mouse":
                execution = new MouseExecution(cmdSubType, postData, desktop);
                break;

            default:
                execution = new AtsExecution(errorCode, true, errorMessage);
                break;
        }
    }

    //TODO: to remove when w3c notation will be used 
    public void Init(string method, int cmdType, int cmdSubType, string[] cmdData, ActionKeyboard keyboard, VisualRecorder recorder, Capabilities capabilities, DesktopManager desktop, bool keyDown = false)
    {
        CommandType type = (CommandType)cmdType;

        if (type == CommandType.Driver)
        {
            execution = new DriverExecution(cmdSubType, cmdData, capabilities, recorder, desktop);
        }
        else if (type == CommandType.Mouse)
        {
            execution = new MouseExecution(cmdSubType, cmdData, desktop);
        }
        else if (type == CommandType.Keyboard)
        {
            execution = new KeyboardExecution(cmdSubType, cmdData, keyboard, keyDown);
        }
        else if (type == CommandType.Window)
        {
            execution = new WindowExecution((WindowType)cmdSubType, cmdData, keyboard, recorder, desktop);
        }
//        else if (type == CommandType.Record)
//        {
//            execution = new RecordExecution(method, cmdSubType, cmdData, recorder);
//        }
        else if (type == CommandType.Element)
        {
            execution = new ElementExecution(cmdSubType, cmdData, desktop);
        }
        else
        {
            execution = new AtsExecution(errorCode, true, errorMessage);
        }
    }
    #endregion

    internal bool Execute(HttpListenerContext context)
    {
        return execution.Run(context);
    }

    void IDisposable.Dispose()
    {

    }
}

public enum DesktopResponseType
{
    AMF = 0,
    atsvFilePath = 1,
    JSON = 2,
    StopServer = -1,
    Text = -2,
    W3CJSON = 3,
    W3CJSONRESPONSE = 4
}

[DataContract(Name = "com.ats.executor.drivers.desktop.DesktopResponse")]

public class DesktopResponse : JsonResponse
{
    public DesktopResponseType type = DesktopResponseType.AMF;
    public string atsvFilePath;

    private DesktopWindow[] _windows = null;
    private DesktopData[] _attributes = null;
    private AtsElement[] _elements = null;

    public DesktopResponse() { }

    public DesktopResponse(HttpListenerContext listener) :base(listener) { }
    public DesktopResponse(int error, bool atsAgent, string message)
    {
        SetError(error, message);
        if (!atsAgent)
        {
            type = DesktopResponseType.Text;
        }
    }

    public void SetError(int code, string message)
    {
        ErrorCode = code;
        ErrorMessage = message;
    }

    public override void Dispose()
    {
        
        Elements = new AtsElement[0];
        //Data = new DesktopData[0];
        _windows = new DesktopWindow[0];
        base.Dispose();
    }
    public void SetWindows(DesktopWindow[] value)
    {
        _windows = value;
    }

    public void SetWindow(DesktopWindow value)
    {
        _windows = new DesktopWindow[] { value };
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    

    [JsonProperty(PropertyName = "windows", NullValueHandling = NullValueHandling.Ignore)]
    [DataMember(Name = "windows", IsRequired = false)]
    public DesktopWindow[] Windows
    {
        get
        {

            if (_windows == null)
            {
                return null;
            }

            List<DesktopWindow> wins = new List<DesktopWindow>();
            for (int i = 0; i < _windows.Length; i++)
            {
                try
                {
                    if (_windows[i].App != null)
                    {
                        wins.Add(_windows[i]);
                    }
                }
                catch { }
            }

            return wins.ToArray();
        }
        set { }
    }

    [JsonProperty(PropertyName = "image", NullValueHandling = NullValueHandling.Ignore)]
    [DataMember(Name = "image", IsRequired = false)]
    public byte[] Image;
        /*
    {
        get
        {
            if (Image == null)
            {
                return null;
            }
            else return Image;
        }
        set
        {
            Image = value;
        }
    }
        */

    [JsonProperty(PropertyName = "elements", NullValueHandling = NullValueHandling.Ignore)]
    [DataMember(Name = "elements", IsRequired = false)]
    //public AtsElement[] Elements;
    
    public AtsElement[] Elements
    {
        get
        {
            if (_elements == null)
            {
                return null;
            }
            else return _elements;
        }
        set
        {
            _elements = value;
        }
    }
    

    [JsonProperty(PropertyName = "attributes", NullValueHandling = NullValueHandling.Ignore)]
    [DataMember(Name = "attributes", IsRequired = false)]
    public DesktopData[] Attributes  
    {
        get
        {
            if (_attributes == null || _attributes.Length == 0)
            {
                return null;
            }
            else return _attributes;
        }
        set
        {
            _attributes = value;
        }
    }

    [JsonProperty(PropertyName = "errorCode")]
    [DataMember(Name = "errorCode", IsRequired = true)]
    public int ErrorCode { get; set; } = 0;
    //public int ErrorCode;

    private string errorMessage = null;

    [JsonProperty(PropertyName = "errorMessage")]
    [DataMember(Name = "errorMessage", IsRequired = false)]
    public string ErrorMessage
    {
        get
        {
            return errorMessage == string.Empty ? null : errorMessage;
        }
        set
        {
            errorMessage = value;
        }
    }

}

[DataContract(Name = "com.ats.executor.TestBound")]
public class TestBound
{
    public TestBound() { }

    public TestBound(double[] elementBound)
    {
        this.X = elementBound[0];
        this.Y = elementBound[1];
        this.Width = elementBound[2];
        this.Height = elementBound[3];
    }

    [JsonProperty(PropertyName = "height")]
    [DataMember(Name = "height")]
    //public double Height;
    public double Height { get; set;}

    [JsonProperty(PropertyName = "width")]
    [DataMember(Name = "width")]
    //public double Width;
        public double Width { get; set; }

    [JsonProperty(PropertyName = "x")]
    [DataMember(Name = "x")]
    //public double X;
        public double X { get; set; }

    [JsonProperty(PropertyName = "y")]
    [DataMember(Name = "y")]
    //public double Y;
    public double Y { get; set; }
}
