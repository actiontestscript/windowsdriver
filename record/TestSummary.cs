﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Runtime.Serialization;
using windowsdriver.record;

[DataContract(Name = "com.ats.recorder.TestSummary")]
public class TestSummary
{

    [DataMember(Name = "actions")]
    public int Actions;

    [DataMember(Name = "error")]
    public TestError TestError;

    [DataMember(Name = "status")]

    public int Status;
    [DataMember(Name = "suiteName")]
    public string SuiteName;

    [DataMember(Name = "summary")]
    public string Summary;

    [DataMember(Name = "testName")]
    public string TestName;

    public TestSummary(bool passed, int actions, string suiteName, string testName, string summary) 
    {
        if (passed)
        {
            this.Status = 1;
        }
        else
        {
            this.Status = 0;
        }

        if (string.IsNullOrEmpty(suiteName))
        {
            suiteName = "noSuiteDefined";
        }

        if (string.IsNullOrEmpty(testName))
        {
            testName = "noTestDefined";
        }

        this.SuiteName = "suite::" + suiteName;
        this.TestName = "script::" + testName;

        this.Actions = actions;

        if (!string.IsNullOrEmpty(summary))
        {
            this.Summary = summary.Replace("\r", "").Replace("\n", "");
        }
    }

    public TestSummary(bool passed, int actions, string suiteName, string testName, string data, TestError error) : this(passed, actions, suiteName, testName, data)
    {
        this.TestError = error;
    }
}