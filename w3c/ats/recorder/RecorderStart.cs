﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.IO;
using System.Net;
using windowsdriver.utils;


namespace windowsdriver.w3c.ats.recorder
{
    /// <summary>
    /// Data for Recorder Start
    /// </summary>
    public class RecorderStartData
    {
        public string id="";                                                                   /*< The id of the recorder start */
        public string fullName = "";                                                           /*< The full name of the recorder start */
        public string description = "";                                                        /*< The description of the recorder start */
        public string author = "";                                                             /*< The author of the recorder start */
        public string groups = "";                                                             /*< The groups of the recorder start */
        public string preRequisites = "";                                                      /*< The preRequisites of the recorder start */
        public string externalId = "";                                                         /*< The externalId of the recorder start */
        public int videoQuality = 2;                                                           /*< The videoQuality of the recorder start */
        public string started;                                                                 /*< The started of the recorder start */
        public string memo;
        public string modifiedAt;
        public string modifiedBy;
    }

    public class RecorderStart : Recorder
    {
        public RecorderStart(HttpListenerContext context, VisualRecorder recorder) :base(context, recorder) { }
        private RecorderStartData datas = new RecorderStartData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas  = JsonConvert.DeserializeObject<RecorderStartData>(jsonValue);
                if(datas.id != null && datas.fullName != null)
                {
                    ActionStart(recorder);
                    SendDesktopResponse();
                    return true;
                }
            }
            SendDesktopResponse();
            return false;
        }

        private void ActionStart(VisualRecorder recorder)
        {
            string tempFolder = Capabilities.GetAtsRecorderTempFolder();
            long freeSpace = 0;

            #region GetFreeSpace
            try
            {
                DriveInfo drive = new DriveInfo(new FileInfo(tempFolder).Directory.Root.FullName);
                freeSpace = drive.AvailableFreeSpace;
            }
            catch { }
            #endregion

            if (freeSpace > 100000000)
            {
                #region directory management
                try
                {
                    if (Directory.Exists(tempFolder))
                    {
                        DirectoryInfo folder = new DirectoryInfo(tempFolder);
                        foreach (FileInfo file in folder.EnumerateFiles())
                        {
                            file.Delete();
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(tempFolder);
                    }
                }
                catch { }
                #endregion

                if (datas.id != null && datas.fullName != null && datas.started != null)
                {
                    recorder.AmfTempFilePath = tempFolder + "\\" + datas.fullName;
                    desktopResponse.atsvFilePath = tempFolder + "\\" + datas.fullName;
                    recorder.Start(tempFolder, datas.id, datas.fullName, datas.description, datas.memo, datas.author, datas.groups, datas.preRequisites, datas.externalId, datas.videoQuality, datas.started, datas.modifiedAt, datas.modifiedBy);
                }

            }
            else
            {
                string fspace = (freeSpace / 1024 / 1024) + " Mo";
//                SetMsgErrorCode("ats_not_enough_space", fspace);
                //response.ErrorCode = -50;
                //response.ErrorMessage = "Not enough space available on disk : " + (freeSpace / 1024 / 1024) + " Mo";
                desktopResponse.ErrorCode = -50;
                desktopResponse.ErrorMessage = "Not enough space available on disk : " + freeSpace ;
                SendDesktopResponse();
            }

        }

    }
}
