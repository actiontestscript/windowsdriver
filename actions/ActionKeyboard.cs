﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

public class ActionKeyboard
{
    private readonly string keyPattern = @"(\$key\([^)]*\))";

    private const string ALT = "ALT";
    private const string SHIFT = "SHIFT";
    private const string CONTROL = "CONTROL";

    private const string BACK_SPACE = "BACK_SPACE";
    private const string BACKSPACE = "BACKSPACE";
    private const string BACK = "BACK";

    private const string END = "END";
    private const string HOME = "HOME";
    private const string PAGE_DOWN = "PAGE_DOWN";
    private const string PAGE_UP = "PAGE_UP";
    private const string INSERT = "INSERT";

    private string[] OTHER_KEYS = { "DELETE", "DEL", "ESC", "LEFT", "RIGHT", "UP", "DOWN", "ENTER", "TAB", END, HOME, PAGE_DOWN, PAGE_UP, BACK, BACK_SPACE, BACKSPACE, INSERT };

    internal void SendKeysData(string data, bool keyDown)
    {
        data = Base64Decode(data);
        if (data.StartsWith("$KEY-", StringComparison.OrdinalIgnoreCase))
        {
            string key = data.Substring(5).ToUpper();
            if (key.Equals(BACK_SPACE) || key.Equals(BACKSPACE) || key.Equals(BACK))
            {
                Keyboard.Type(VirtualKeyShort.BACK);
            }
            else if (key.Equals(PAGE_DOWN))
            {
                Keyboard.Type(VirtualKeyShort.NEXT);
            }
            else if (key.Equals(PAGE_UP))
            {
                Keyboard.Type(VirtualKeyShort.PRIOR);
            }
            else if (key.Equals(INSERT))
            {
                Keyboard.Type(VirtualKeyShort.INSERT);
            }
            else
            {
                SendKeys.SendWait("{" + key + "}");
            }
        }
        else
        {
            if (keyDown)
            {
                Keyboard.Type(data.ToLowerInvariant());
            }
            else
            {
                KeyBoardType(data);
            }
        }
    }

    internal void Clear(AtsElement element)
    {
        if (element != null)
        {
            element.TextClear();
        }
    }

    internal void RootKeys(string keys)
    {
        string[] tokens = Regex.Split(keys, keyPattern);
        foreach (string token in tokens)
        {
            if (token.Length > 0)
            {
                string data = token;
                if (token.StartsWith("$key", StringComparison.OrdinalIgnoreCase))
                {
                    string addKeys = "";
                    data = token.Substring(5, token.Length - 6);

                    if (data.Contains(CONTROL + "-"))
                    {
                        addKeys += "^";
                        data = data.Replace(CONTROL + "-", "");
                    }

                    if (data.Contains(SHIFT + "-"))
                    {
                        addKeys += "+";
                        data = data.Replace(SHIFT + "-", "");
                    }

                    if (data.Contains(ALT + "-"))
                    {
                        addKeys += "%";
                        data = data.Replace(ALT + "-", "");
                    }

                    if (Array.IndexOf(OTHER_KEYS, data) > -1)
                    {
                        if (data.Equals(BACK_SPACE) || data.Equals(BACK) || data.Equals(BACKSPACE))
                        {
                            data = BACKSPACE;
                        }
                        else if (data.Equals(PAGE_UP))
                        {
                            Keyboard.Type(VirtualKeyShort.PRIOR);
                            return;
                        }
                        else if (data.Equals(PAGE_DOWN))
                        {
                            Keyboard.Type(VirtualKeyShort.NEXT);
                            return;
                        }
                        else if (data.Equals(INSERT))
                        {
                            Keyboard.Type(VirtualKeyShort.INSERT);
                            return;
                        }
                    }
                    else
                    {
                        data = data.ToLower();
                    }

                    SendKeysData(addKeys + "{" + data + "}");
                }
                else
                {
                    KeyBoardType(token);
                }
            }
        }
    }

    private void SendKeysData(string data)
    {
        try
        {
            SendKeys.SendWait(@data);
        }
        catch { }
    }

    private void KeyBoardType(string data)
    {
        foreach (char c in data)
        {
            Keyboard.Type(c);
            Thread.Sleep(20);
        }
    }

    internal void Down(string code)
    {
        if ("33".Equals(code) || "57353".Equals(code))//ctrl key
        {
            Keyboard.Press(VirtualKeyShort.CONTROL);
        }
        else if ("46".Equals(code) || "57352".Equals(code))//shift key
        {
            Keyboard.Press(VirtualKeyShort.SHIFT);
        }
        else if ("57354".Equals(code))//alt key
        {
            Keyboard.Press(VirtualKeyShort.ALT);
        }
    }

    internal void Release(string code)
    {
        if ("33".Equals(code) || "57353".Equals(code))//ctrl key
        {
            Keyboard.Release(VirtualKeyShort.CONTROL);
        }
        else if ("46".Equals(code) || "57352".Equals(code))//shift key
        {
            Keyboard.Release(VirtualKeyShort.SHIFT);
        }
        else if ("57354".Equals(code))//alt key
        {
            Keyboard.Release(VirtualKeyShort.ALT);
        }
    }

    private static string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
        return Encoding.UTF8.GetString(base64EncodedBytes);
    }
}