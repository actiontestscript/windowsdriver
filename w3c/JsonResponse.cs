﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using windowsdriver.Utils.Logger;
using static windowsdriver.w3c.JsonResponseData;

namespace windowsdriver.w3c
{
    public class JsonResponse : IDisposable
    {
        protected static readonly string GET = "GET";
        protected static readonly string DELETE = "DELETE";
        protected static readonly string POST = "POST";

        private HttpListenerContext listener;
        public string jsonValue { get; private set; }

        protected string method = "";
        protected List<string> urlData = new List<string>();
        protected dynamic postedData;
        protected string dataPostValue = "";
        public object value;
        protected int httpCode = 200;
        protected string m_errorCode = "";
        protected string m_errorMessage = "";
        protected string m_stackTrace = "";
        protected string m_datas = "";
        public int error = -1;

        public JsonResponse()
        {
            this.listener = null;
        }
        public JsonResponse(HttpListenerContext listener)
        {
            this.listener = listener;

            using (var reader = new StreamReader(listener.Request.InputStream, listener.Request.ContentEncoding)) dataPostValue  = reader.ReadToEnd();

            try
            {
                postedData = JsonConvert.DeserializeObject(dataPostValue);
            }
            catch
            {
                postedData = new object();
            }
        }

        public JsonResponse(HttpListenerContext listener, string jsonData )
        {
            this.listener = listener;
            this.dataPostValue = jsonData;

            try
            {
                postedData = JsonConvert.DeserializeObject(jsonData);
            }
            catch
            {
                postedData = new object();
            }
        }

        public void SetContextVariable()
        {
            this.method = this.listener.Request.HttpMethod;
            this.urlData = this.listener.Request.Url.AbsolutePath.Substring(1).Split('/').ToList();
        }

        public void SetHttpListenerContext(HttpListenerContext context)
        {
            this.listener = context;
        }

        public void SetValue(object value)
        {
            this.value = value;
        }

        /// <summary>
        /// Check if json containt a value key and extract json data value in jsonValue
        /// </summary>
        /// <returns>true if json containt value and false if not containt key value or error value and send error Message in json W3C format</returns>
        public bool SetJsonValue()
        {
            if (listener.Request.HttpMethod == "POST")
            {
                try
                {
                    JObject json = JObject.Parse(dataPostValue);
                    if (json.ContainsKey("value"))
                    {
                        jsonValue = json["value"].ToString();
                        return true;
                    }
                    SetMsgErrorCode("ats_json_data_null");
                    Send();
                    return false;
                }
                catch (JsonException ex)
                {
                    AtsLogger.WriteError("invalide JSON -> " + ex.Message);
                    SetMsgErrorCode("ats_json_data_null" + ex.Message);
                    Send();
                    return false;
                }
            }
            return true;
        }
        public virtual void Send(object value)
        {
            this.value = value;
            if(value is DesktopResponse)
            {
                //check if error code is define
                DesktopResponse desktopResponse = (DesktopResponse)value;
                if (desktopResponse.ErrorCode != 0)
                {
                    SetAtsDesktopResponse();
                }
            }
            Send();
        }

        public virtual void Send()
        {
            try
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Include;  //don't not modified : NullValueHandling.Include;
                byte[] bytes;

                using (MemoryStream mem = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(mem, Encoding.UTF8))
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        var responseObj = new { value = this.value };
                        serializer.Serialize(writer, responseObj);
                        sw.Flush();
                        mem.Position = 0;
                        bytes = mem.ToArray();
                    }
                }

                if (listener != null)
                {
                    listener.Response.StatusCode = httpCode;
                    listener.Response.ContentType = "application/json; charset=utf-8";
                    listener.Response.ContentLength64 = bytes.Length;
                    listener.Response.AddHeader("Cache-Control", "no-cache");
                    listener.Response.OutputStream.Write(bytes, 0, bytes.Length);
                    listener.Response.OutputStream.Close();
                    listener.Response.Close();
                }
                else
                {
                    AtsLogger.WriteError("JsonResponse listener cannot be null." );
                }
            }
            catch (Exception ex)
            {
                AtsLogger.WriteError("unknown error -> " + ex.Message);
            }
            finally
            {
                listener?.Response?.Close();
            }

            value = null;
            listener = null;
        }
        
        public HttpListenerContext getListener() { return listener; }

        public String getHttpMethod()
        {
            return this.listener.Request.HttpMethod;
        }
        public Boolean isValueDefine()
        {
            return this.value != null;
        }

        public void SetAtsDesktopResponse()
        {
            JsonResponseData responseDatas = new JsonResponseData();
            ErrorCode errorCode = responseDatas.getErrorCode("ats_desktop_response_error");
            DesktopResponse desktopResponse = (DesktopResponse ) this.value;
            httpCode = errorCode.HttpCode;
            m_errorCode = errorCode.JsonErrorCode;
            m_errorMessage = errorCode.MessageErrorCode;
            this.value = new JsonResponseDesktopResponseErrorValue(m_errorCode, m_errorMessage, m_stackTrace, desktopResponse);
            //JsonResponseDesktopResponseErrorValue
            /*
            this.value = new JsonResponseDesktopResponseErrorValue(m_errorCode, m_errorMessage, m_stackTrace, m_datas);
            
            SetMsgErrorCode("ats_desktop_response_error", "", desktopResponse);
            */

        }

        /// <summary>
        /// Set the error code and message from the error code table
        /// </summary>
        /// <param name="strKey"></param>
        public void SetMsgErrorCode(string strKey)
        {
            SetMsgErrorCode(strKey, null, null);
        }

        /// <summary>
        /// Set the error code and message from the error code table and add the stack trace
        /// </summary>
        /// <param name="strKey"></param>
        /// <param name="stackTrace"></param>
        public void SetMsgErrorCode(string strKey, string stackTrace)
        {
            SetMsgErrorCode(strKey, stackTrace, null);
        }

        /// <summary>
        /// Set the error code and message from the error code table and add the stack trace and datas
        /// </summary>
        /// <param name="strKey"></param>
        /// <param name="stackTrace"></param>
        /// <param name="datas"></param>
        public void SetMsgErrorCode(string strKey, string stackTrace, string datas)
        {
            JsonResponseData responseDatas = new JsonResponseData();
            ErrorCode errorCode = responseDatas.getErrorCode(strKey);
            if (errorCode != null)
            {
                httpCode = errorCode.HttpCode;
                m_errorCode = errorCode.JsonErrorCode;
                m_errorMessage = errorCode.MessageErrorCode;
            }
            else
            {
                httpCode = 500;
                m_errorCode = "unknown error";
                m_errorMessage = "unknown error";
            }
            if(stackTrace != null) m_stackTrace = stackTrace;
            if (datas != null) m_datas = datas;

            this.value = new JsonResponseErrorValue(m_errorCode, m_errorMessage,m_stackTrace,m_datas);
        }

        /// <summary>
        /// Get the json response for an error
        /// </summary>
        /// <returns>json in string format</returns>
        public string GetJsonResponseError()
        {
            
            var jsonError = new Dictionary<string, object>
            {
                {"error", m_errorCode },
                {"message", m_errorMessage },
                {"stacktrace", m_stackTrace },
                {"data", m_datas }
            };
            var jsonValue = new Dictionary<string, object>
            {
                {"value", jsonError }
            };

            return JsonConvert.SerializeObject(jsonValue);
        }

        public virtual void Dispose(){ }
    }



    public class JsonResponseErrorValue
    {
        public string error;
        public string message;
        public string stackTrace;
        public string datas;
        
        public JsonResponseErrorValue(string error, string message, string stackTrace, string datas)
        {
            this.error = error;
            this.message = message;
            this.stackTrace = stackTrace;
            this.datas = datas;
        }
    }

    public class JsonResponseDesktopResponseErrorValue
    {
        public string error;
        public string message;
        public string stackTrace;
        public DesktopResponse datas;

        public JsonResponseDesktopResponseErrorValue(string error, string message, string stackTrace, DesktopResponse datas)
        {
            this.error = error;
            this.message = message;
            this.stackTrace = stackTrace;
            this.datas = datas;
        }
    }
}
