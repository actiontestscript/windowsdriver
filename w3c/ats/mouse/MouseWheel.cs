﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;

namespace windowsdriver.w3c.ats.mouse
{
    class MouseWheelData
    {
        public int delta = -1;
    }
    class MouseWheel : Mouse
    {
        public MouseWheel(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private MouseWheelData datas = new MouseWheelData();

        public override bool Execute()
        {
            if (base.Execute()) { 
                datas = JsonConvert.DeserializeObject<MouseWheelData>(jsonValue);
                if (base.Execute())
                {
                    int delta = datas.delta;
                    FlaUI.Core.Input.Mouse.Scroll(-delta / 100);
                }
            }
            Send();
            return false;
        }
    }
}
