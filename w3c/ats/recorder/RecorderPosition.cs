﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Net;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderPositionData
    {
        public string hPos =  null;
        public string hPosValue = null;
        public string vPos = null;
        public string vPosValue = null;
    }
    class RecorderPosition : Recorder
    {
        public RecorderPosition(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderPositionData datas = new RecorderPositionData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderPositionData>(jsonValue);
                if (datas.hPos != null && datas.vPos != null)
                {
                    AtsLogger.WriteAll("record-position -> hpos={0}, vpos={1}", datas.hPosValue, datas.vPosValue);

                    try
                    {
                        recorder.AddPosition(datas.hPos, datas.hPosValue, datas.vPos, datas.vPosValue);
                    }
                    catch (Exception ex)
                    {
                        AtsLogger.WriteError("record-position-error -> {0}", ex.Message);
                        SetMsgErrorCode("ats_recorder_position_failed", ex.Message);
                    }
                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }
}
