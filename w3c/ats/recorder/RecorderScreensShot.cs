﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using windowsdriver.Utils.Logger;

namespace windowsdriver.w3c.ats.recorder
{
    class RecorderScreenShotData
    {
//        public DriverInfo driverInfo;
        public int x = 0;
        public int y = 0;
        public int w = 1;
        public int h = 1;
    }
    class RecorderScreenShot : Recorder
    {
        public RecorderScreenShot(HttpListenerContext context, VisualRecorder recorder) : base(context, recorder) { }
        private RecorderScreenShotData datas = new RecorderScreenShotData();
        private DesktopResponse response = new DesktopResponse();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<RecorderScreenShotData>(jsonValue);
                AtsLogger.WriteAll("record-screenshot -> bound=[{0}, {1}, {2}, {3}]", datas.x, datas.y, datas.w, datas.h);
                try
                {
                    response.Image = VisualRecorder.ScreenCapturePng(datas.x, datas.y, datas.w, datas.h);
                    SetValue(response);
                }
                catch (Exception ex)
                {
                    AtsLogger.WriteError("record-screenshot-error -> {0}", ex.Message);
                }
                Send();
                return true;
            }
            Send();
            return false;
        }

    }
}
