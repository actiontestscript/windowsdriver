﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;

namespace executions.DataConverter
//namespace windowsdriver.executions.DataConverter
{
    /*
    internal class RecordDC : DataConverter
    {
        internal RecordDC(string[] data) : base(data) { }

        internal RecordDC(string data) : base(data) { }

        #region GetDataElement
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal (DtoElement, string, int) GetDataElement()
        {
            DtoElement dto = null;
            string responseMsg = null;
            int responseCode = (int)HttpStatusCode.OK;
            string searchCriterias = "";

            if (dataStr != null)
            {
                var obj = new
                {
                    value = new DtoElement()
                };
                try
                {
                    dto = JsonConvert.DeserializeAnonymousType(dataStr, obj).value;

                    if (dto.searchCriterias != null && dto.searchCriterias != string.Empty)
                    {
                        searchCriterias = dto.searchCriterias;
                    }
                    (dto.tag, dto.searchCriterias) = GetCriterias(searchCriterias);

                    responseMsg = base.SerialiseSuccess();
                }
                catch (JsonException ex)
                {
                    responseCode = 400;
                    responseMsg = SerialiseError(ex.Message, responseCode);
                }
            }
            else
            {
                #region ArrayToDto
                if (dataTab.Length > 6)
                {
                    searchCriterias = dataTab[6];
                }

                string tag = "*";
                string[] citeriasData = searchCriterias.Split(',');
                if (citeriasData.Length > 0)
                {
                    tag = citeriasData[0];
                    if (citeriasData.Length == 1)
                    {
                        searchCriterias += "(no-criterias)";
                    }
                }

                dto = new DtoElement()
                {
                    elementBound = new double[] { 0, 0, 0, 0 },
                };
                (dto.tag, dto.searchCriterias) = GetCriterias(searchCriterias);

                _ = double.TryParse(dataTab[0], out dto.elementBound[0]);
                _ = double.TryParse(dataTab[1], out dto.elementBound[1]);
                _ = double.TryParse(dataTab[2], out dto.elementBound[2]);
                _ = double.TryParse(dataTab[3], out dto.elementBound[3]);

                _ = long.TryParse(dataTab[4], out dto.searchDuration);
                _ = int.TryParse(dataTab[5], out dto.numElements);
                #endregion
            }
            return (dto, responseMsg, responseCode);
        }

        private (string, string) GetCriterias(string searchCriterias)
        {
            string tag = "*";
            string[] citeriasData = searchCriterias.Split(',');
            if (citeriasData.Length > 0)
            {
                tag = citeriasData[0];
                if (citeriasData.Length == 1)
                {
                    searchCriterias += "(no-criterias)";
                }
            }
            return (tag, searchCriterias);
        }
        #endregion

        #region ArrayToDtoStart
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>typed dto</returns>
        internal DtoStart ArrayToDtoStart(string[] dataTab)
        {
            DtoStart dto = new DtoStart()
            {
                id = dataTab[0],
                fullName = dataTab[1],
                description = dataTab[2],
                author = dataTab[3],
                groups = dataTab[4],
                prereq = dataTab[5],
                externalId = dataTab[6],
                videoQuality = int.Parse(dataTab[7]),
                started = dataTab[8]
            };
            return (dto);
        }
        #endregion

        #region ArrayToDtoCreate
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoCreate ArrayToDtoCreate(string[] dataTab)
        {
            double[] channelDimmension = new double[] { 0, 0, 1, 1 };
            double.TryParse(dataTab[5], out channelDimmension[0]);
            double.TryParse(dataTab[6], out channelDimmension[1]);
            double.TryParse(dataTab[7], out channelDimmension[2]);
            double.TryParse(dataTab[8], out channelDimmension[3]);

            bool.TryParse(dataTab[9], out bool sync);
            bool.TryParse(dataTab[10], out bool stop);

            int.TryParse(dataTab[1], out int line);
            long.TryParse(dataTab[3], out long timeLine);

            DtoCreate dto = new DtoCreate()
            {
                actionType = dataTab[0],
                line = line,
                script = dataTab[2],
                timeLine = timeLine,
                channelName = dataTab[4],
                channelDimension = channelDimmension,
                sync = sync,
                stop = stop
            };
            return dto;
        }
        #endregion

        #region ArrayToDtoCreateMobile
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoCreateMobile ArrayToDtoCreateMobile(string[] dataTab)
        {
            DtoCreateMobile dto = new DtoCreateMobile()
            {
                actionType = dataTab[0],
                script = dataTab[2],
                channelName = dataTab[4],
                channelDimension = new double[] { 0, 0, 1, 1 },
                url = dataTab[9],
            };

            _ = int.TryParse(dataTab[1], out dto.line);
            _ = long.TryParse(dataTab[6], out dto.timeLine);
            _ = bool.TryParse(dataTab[10], out dto.sync);
            _ = bool.TryParse(dataTab[11], out dto.stop);

            _ = double.TryParse(dataTab[5], out dto.channelDimension[0]);
            _ = double.TryParse(dataTab[6], out dto.channelDimension[1]);
            _ = double.TryParse(dataTab[7], out dto.channelDimension[2]);
            _ = double.TryParse(dataTab[8], out dto.channelDimension[3]);

            return dto;
        }
        #endregion

        #region ArrayToDtoImage
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoImage ArrayToDtoImage(string[] dataTab)
        {
            DtoImage dto = new DtoImage()
            {
                screenRect = new double[] { 0, 0, 1, 1 }
            };
            _ = double.TryParse(dataTab[0], out dto.screenRect[0]);
            _ = double.TryParse(dataTab[1], out dto.screenRect[1]);
            _ = double.TryParse(dataTab[2], out dto.screenRect[2]);
            _ = double.TryParse(dataTab[3], out dto.screenRect[3]);
            _ = bool.TryParse(dataTab[4], out dto.isRef);

            return dto;
        }
        #endregion

        #region ArrayToDtoImageMobile
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoImageMobile ArrayToDtoImageMobile(string[] dataTab)
        {
            DtoImageMobile dto = new DtoImageMobile()
            {
                screenRect = new double[] { 0, 0, 1, 1 },
                url = dataTab[5]
            };

            _ = bool.TryParse(dataTab[4], out dto.isRef);
            _ = double.TryParse(dataTab[0], out dto.screenRect[0]);
            _ = double.TryParse(dataTab[1], out dto.screenRect[1]);
            _ = double.TryParse(dataTab[2], out dto.screenRect[2]);
            _ = double.TryParse(dataTab[3], out dto.screenRect[3]);

            return dto;
        }
        #endregion

        #region ArrayToDtoValue
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoValue ArrayToDtoValue(string[] dataTab)
        {
            return new DtoValue()
            {
                v = dataTab[0]
            };
        }
        #endregion

        #region ArrayToDtoData
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoData ArrayToDtoData(string[] dataTab)
        {
            return new DtoData()
            {
                v1 = dataTab[0],
                v2 = dataTab[1]
            };
        }
        #endregion

        #region ArrayToDtoStatus
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoStatus ArrayToDtoStatus(string[] dataTab)
        {

            DtoStatus dto = new DtoStatus();
            _ = int.TryParse(dataTab[0], out dto.error);
            _ = long.TryParse(dataTab[1], out dto.duration);

            return dto;
        }
        #endregion

        #region ArrayToDtoPosition
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoPosition ArrayToDtoPosition(string[] dataTab)
        {
            return new DtoPosition()
            {
                hpos = dataTab[0],
                hposValue = dataTab[1],
                vpos = dataTab[2],
                vposValue = dataTab[3]
            };
        }
        #endregion

        #region ArrayToDtoSummary
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoSummary ArrayToDtoSummary(string[] dataTab)
        {
            if (dataTab.Count() < 6)
            {
                Array.Resize<string>(ref dataTab, 8);
                dataTab[5] = string.Empty;
                dataTab[6] = "0";
                dataTab[7] = string.Empty;
            }

            DtoSummary dto = new DtoSummary()
            {
                suiteName = dataTab[2],
                testName = dataTab[3],
                data = dataTab[4],
                errorScript = dataTab[5],
                errorMessage = dataTab[7]
            };

            bool.TryParse(dataTab[0], out dto.passed);
            int.TryParse(dataTab[1], out dto.actions);
            int.TryParse(dataTab[6], out dto.errorLine);

            return dto;
        }
        #endregion

        #region ArrayToDtoScreenshot
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoScreenshot ArrayToDtoScreenshot(string[] dataTab)
        {
            DtoScreenshot dto = new DtoScreenshot();
            if (int.TryParse(dataTab[0], out int x) &&
                int.TryParse(dataTab[1], out int y) &&
                int.TryParse(dataTab[2], out int w) &&
                int.TryParse(dataTab[3], out int h))
            {
                dto = new DtoScreenshot()
                {
                    x = x,
                    y = y,
                    w = w,
                    h = h
                };
            };
            return dto;
        }
        #endregion

        #region ArrayToDtoScreenshotMobile
        /// <summary>
        /// Get data from the input source 
        /// </summary>
        /// <returns>data/response message/response code</returns>
        internal DtoScreenshotMobile ArrayToDtoScreenshotMobile(string[] dataTab)
        {
            return new DtoScreenshotMobile()
            {
                uri = dataTab[0]
            };
        }
        #endregion

        #region dto
        internal class DtoStart : IDto
        {
            public DtoStart() { }

            [JsonProperty("id")]
            internal string id;

            [JsonProperty("fullName")]
            internal string fullName;

            [JsonProperty("description")]
            internal string description;

            [JsonProperty("author")]
            internal string author;

            [JsonProperty("groups")]
            internal string groups;

            [JsonProperty("preRequisites")]
            internal string prereq;

            [JsonProperty("externalId")]
            internal string externalId;

            [JsonProperty("videoQuality")]
            internal int videoQuality;

            [JsonProperty("started")]
            internal string started;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

  
        }

        internal class DtoCreate : IDto
        {
            [JsonProperty("actionType")]
            internal string actionType;

            [JsonProperty("line")]
            internal int line;

            [JsonProperty("script")]
            internal string script;

            [JsonProperty("timeLine")]
            internal long timeLine;

            [JsonProperty("channelName")]
            internal string channelName;

            [JsonProperty("channelDimension")]
            internal double[] channelDimension;

            [JsonProperty("sync")]
            internal bool sync;

            [JsonProperty("stop")]
            internal bool stop;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo driverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoCreateMobile : IDto
        {
            [JsonProperty("actionType")]
            internal string actionType;

            [JsonProperty("line")]
            internal int line;

            [JsonProperty("script")]
            internal string script;

            [JsonProperty("timeline")]
            internal long timeLine;

            [JsonProperty("channelName")]
            internal string channelName;

            [JsonProperty("channelDimension")]
            internal double[] channelDimension;

            [JsonProperty("url")]
            internal string url;

            [JsonProperty("sync")]
            internal bool sync;

            [JsonProperty("stop")]
            internal bool stop;

        }

        internal class DtoImage : IDto
        {
            [JsonProperty("screenRect")]
            internal double[] screenRect;

            [JsonProperty("isRef")]
            internal bool isRef;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoImageMobile : IDto
        {
            [JsonProperty("screenRect")]
            internal double[] screenRect;

            [JsonProperty("isRef")]
            internal bool isRef;

            [JsonProperty("url")]
            internal string url;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoValue : IDto
        {
            [JsonProperty("v")]
            internal string v;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoData : IDto
        {
            [JsonProperty("v1")]
            internal string v1;

            [JsonProperty("v2")]
            internal string v2;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoStatus : IDto
        {
            [JsonProperty("error")]
            internal int error;

            [JsonProperty("duration")]
            internal long duration;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoElement : IDto
        {
            [JsonProperty("elementBound")]
            internal double[] elementBound;

            [JsonProperty("searchDuration")]
            internal long searchDuration;

            [JsonProperty("numElements")]
            internal int numElements;

            [JsonProperty("searchCriterias")]
            internal string searchCriterias;

            internal string tag;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoPosition : IDto
        {
            [JsonProperty("hpos")]
            internal string hpos;

            [JsonProperty("hposValue")]
            internal string hposValue;

            [JsonProperty("vpos")]
            internal string vpos;

            [JsonProperty("vposValue")]
            internal string vposValue;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoSummary : IDto
        {
            [JsonProperty("passed")]
            internal bool passed;

            [JsonProperty("actions")]
            internal int actions;

            [JsonProperty("suiteName")]
            internal string suiteName;

            [JsonProperty("testName")]
            internal string testName;

            [JsonProperty("data")]
            internal string data;

            [JsonProperty("errorScript")]
            internal string errorScript;

            [JsonProperty("errorLine")]
            internal int errorLine;

            [JsonProperty("errorMessage")]
            internal string errorMessage;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoScreenshot : IDto
        {
            [JsonProperty("x")]
            internal int x;

            [JsonProperty("y")]
            internal int y;

            [JsonProperty("w")]
            internal int w;

            [JsonProperty("h")]
            internal int h;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }

        internal class DtoScreenshotMobile : IDto
        {
            [JsonProperty("uri")]
            internal string uri;

            // DriverInfo
            [JsonProperty("driverInfo")]
            internal DriverInfo DriverInfo { get; set; }

            [JsonProperty("sessionId")]
            internal string sessionId;

            [JsonProperty("screenshotUrl")]
            internal string screenshotUrl;

            [JsonProperty("headless")]
            internal bool headless;

        }
        #endregion
    }
    */
}
