﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.WindowsAPI;
using Newtonsoft.Json;
using System;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Definitions;
using FlaUI.Core.Input;
using System.IO;
using System.Net;

namespace windowsdriver.w3c.ats.windows
{
    public class WindowUrlData
    {
        public int handle = -1;
        public string url = null;
    }

    public class WindowUrl : Window
    {
        public WindowUrl(HttpListenerContext context, ActionKeyboard keyboard, DesktopManager desktop, VisualRecorder recorder) : base(context, keyboard, desktop, recorder) { }
        private WindowUrlData datas = new WindowUrlData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<WindowUrlData>(jsonValue);
                if (datas.handle != -1 && datas.url != null)
                {
                    window = desktop.GetWindowByHandle(datas.handle);
                    if (window.isIE)
                    {
                        window.ToFront();

                        AutomationElement win = window.Element;
                        AutomationElement addressBar = win.FindFirst(TreeScope.Descendants, win.ConditionFactory.ByControlType(ControlType.Pane).And(win.ConditionFactory.ByClassName("Address Band Root")));
                        if (addressBar != null)
                        {
                            addressBar.Focus();
                            AutomationElement edit = addressBar.FindFirstChild(addressBar.ConditionFactory.ByControlType(ControlType.Edit));

                            if (edit != null)
                            {
                                try
                                {
                                    edit.Focus();
                                    edit.Click();
                                    if (edit.Patterns.Value.IsSupported)
                                    {
                                        edit.Patterns.Value.Pattern.SetValue(datas.url);
                                    }
                                    Keyboard.Type(VirtualKeyShort.ENTER);
                                }
                                catch {
                                    SendDesktopResponse(window);
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (WindowExecution.REFRESH.Equals(datas.url) || WindowExecution.NEXT.Equals(datas.url) || WindowExecution.BACK.Equals(datas.url))
                        {
                            window.Navigate(datas.url, desktop);
                        }
                        else
                        {

                            string fname = Environment.ExpandEnvironmentVariables(datas.url);
                            if (fname.StartsWith("file:///"))
                            {
                                fname = fname.Substring(8);
                            }

                            try
                            {
                                FileAttributes attr = File.GetAttributes(@fname);
                                if (!attr.HasFlag(FileAttributes.Directory))
                                {
                                    DirectoryInfo dirInfo = Directory.GetParent(@fname);
                                    fname = dirInfo.FullName;
                                }

                                if (Directory.Exists(@fname))
                                {
                                    window.GotoUrl(Path.GetFullPath(@fname), desktop);
                                }
                                else
                                {
                                    desktopResponse.SetError(UNREACHABLE_GOTO_URL, "directory not found : " + fname);
                                    SendDesktopResponse(window);
                                    return true;
                                }
                            }
                            catch (Exception e)
                            {
                                desktopResponse.SetError(UNREACHABLE_GOTO_URL, "directory path not valid : " + fname + " (" + e.Message + ")");
                                SendDesktopResponse(window);
                                return false;
                            }
                        }
                    }
                    SendDesktopResponse(window);
                    return true;
                }
            }
            SendDesktopResponse();
            return false;
        }
    }
}