﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.session.alert
{
    public class AlertFactory
    {
        public AlertCommand Create(SessionW3c.SessionW3CType? sessionW3CType, HttpListenerContext context)
        {
            switch (sessionW3CType)
            {
                case SessionW3c.SessionW3CType.AlertDismiss:
                    return new AlertDismiss(context);
                case SessionW3c.SessionW3CType.AlertAccept:
                    return new AlertAccept(context);
                case SessionW3c.SessionW3CType.AlertText:
                    return new AlertText(context);
                default:
                    return new AlertOthers(context);
            }
        }
    }
}
