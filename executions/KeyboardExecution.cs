﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Collections.Generic;
using System.Net;
using windowsdriver.items;

class KeyboardExecution : AtsExecution
{
    private const int errorCode = -6;

    private enum KeyType
    {
        Clear = 0,
        Enter = 1,
        Down = 2,
        Release = 3
    };

    Dictionary<string, int> keyTypeDict = new Dictionary<string, int>()
    {
        {"clear", 0},
        {"enter", 1},
        {"down", 2},
        {"release", 3}
    };

    private ActionKeyboard action;
    private string data;
    private string id;
    private bool keyDown;
    private KeyType type;

    #region Constructor
    public KeyboardExecution(int stype, string[] commandsData, ActionKeyboard action, bool keyDown) : base()
    {
        KeyboardExec(stype, commandsData, action, keyDown);
    }

    public KeyboardExecution(string stype, string postData, ActionKeyboard action, bool keyDown) : base()
    {
        response.SetError(500, "Not implemented");
        return;
        /*
        string[] commandsData = new List<string>().ToArray();

        if (keyTypeDict.TryGetValue(stype.ToLower(), out int sTypeVal))
        {
            KeyboardExec(sTypeVal, commandsData, action, keyDown);
        }
        else
        {
            //TODO: Send exception
        }
        */
    }
    #endregion

    #region KeyboardExec
    public void KeyboardExec(int stype, string[] commandsData, ActionKeyboard action, bool keyDown)
    {
        // keyTypeDict.TryGetValue(stype.ToLower(), out int sTypeVal);
        this.action = action;
        this.type = (KeyType)stype;
        this.keyDown = keyDown;

        if (commandsData.Length > 0)
        {
            data = commandsData[0];
        }
        if (commandsData.Length > 1 && commandsData[1] != "")
        {
            id = commandsData[1];
        }
    }
    #endregion

    public override bool Run(HttpListenerContext context)
    {
        if (type == KeyType.Clear)
        {
            if (data != null)
            {
                action.Clear(CachedElements.Instance.GetElementById(data));
            }
            else
            {
                action.Clear(null);
            }
        }
        else if (data != null)
        {
            if (type == KeyType.Enter)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    AtsElement element = CachedElements.Instance.GetElementById(id);
                    if (element != null)
                    {
                        element.Focus();
                    }
                }
                action.SendKeysData(data, keyDown);
            }
            else if (type == KeyType.Down)
            {
                action.Down(data);
            }
            else if (type == KeyType.Release)
            {
                action.Release(data);
            }
            else
            {
                response.SetError(errorCode, "unknown text command");
            }
        }
        else
        {
            response.SetError(errorCode, "enter text data command error");
        }

        return base.Run(context);
    }
}