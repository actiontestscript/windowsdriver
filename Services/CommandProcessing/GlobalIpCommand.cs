﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
using System.Net;
using System;
using windowsdriver.utils;
using System.Linq;

namespace windowsdriver.Services.CommandProcessing
{
    public class GlobalIpCommand : CommandBase
    {
        public override string CommandType => "globalip=";
        public override string HelpGlobal => "--globalIp=IPV4\t\tpublic Ip access on remote server";
        public override string HelpDetails => "--globalIp=IPV4\t\tpublic Ip access on remote server";



        public override void Execute(string command, CommandResult commandResult)
        {
            var ipsData = command.Split('=');
            if (ipsData.Length == 1) return;

            if (ipsData[1].Length > 0)
            {
                commandResult.globalIp = ipsData[1];
                commandResult.allowedIps.Add(ipsData[1]);
            }
            else
            {
                Console.WriteLine("invalid global ip");
                Environment.Exit(-1);
            }
        }
    }
}
