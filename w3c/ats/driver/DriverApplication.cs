﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using windowsdriver.utils;
using FlaUI.Core;

namespace windowsdriver.w3c.ats.driver
{
    public class DriverApplicationData
    {
        public string args = null;
        public bool attach = false;
    }
    public class DriverApplication : Driver
    {
        public DriverApplication(HttpListenerContext context, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop) : base(context, capabilities, recorder, desktop) { }
        private DriverApplicationData datas = new DriverApplicationData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<DriverApplicationData>(jsonValue);
                if (datas.args != null)
                {
                    bool attach = datas.attach;
                    string[] args = datas.args.Split('\n');
                    string appName = args[0];

                    int protocoleSplitIndex = appName.IndexOf("://");
                    if (protocoleSplitIndex > 0)
                    {
                        string applicationProtocol = appName.Substring(0, protocoleSplitIndex).ToLower();
                        string applicationData = appName.Substring(protocoleSplitIndex + 3);

                        if (applicationProtocol.Equals(UWP_PROTOCOLE))
                        {
                            string[] uwpUrlData = applicationData.Split('/');
                            if (uwpUrlData.Length > 1)
                            {
                                string packageName = uwpUrlData[0];
                                string windowName = uwpUrlData[1];

                                if (windowName.Length > 0)
                                {
                                    string appId = "App";
                                    int exclamPos = packageName.IndexOf("!");
                                    if (exclamPos > -1)
                                    {
                                        appId = packageName.Substring(exclamPos + 1);
                                        packageName = packageName.Substring(0, exclamPos);
                                    }

                                    if (!packageName.Contains("_"))
                                    {
                                        packageName = UwpApplications.getApplicationId(packageName);
                                    }

                                    if (packageName != null)
                                    {
                                        try
                                        {
                                            Application app = Application.LaunchStoreApp(packageName + "!" + appId);
                                            //app.WaitWhileBusy(TimeSpan.FromSeconds(7));
                                            //app.WaitWhileMainHandleIsMissing(TimeSpan.FromSeconds(7));

                                            Process uwpProcess = Process.GetProcessById(app.ProcessId);

                                            if (uwpProcess != null)
                                            {
                                                int maxTry = 5;
                                                DesktopWindow window = null;
                                                while (window == null && maxTry > 0)
                                                {
                                                    System.Threading.Thread.Sleep(100);
                                                    window = desktop.GetWindowByTitle(windowName);
                                                    maxTry--;
                                                }

                                                if (window != null)
                                                {
                                                    window.UpdateApplicationData(uwpProcess);
                                                    SendDesktopResponse(window);
                                                    Dispose();
                                                    return true;
                                                }
                                                else
                                                {
                                                    desktopResponse.SetError(errorCode, "window with name '" + windowName + "' not found");
                                                    SendDesktopResponse();
                                                    Dispose();
                                                    return false;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            desktopResponse.SetError(errorCode, "cannot start UWP application : " + e.Message);
                                            SendDesktopResponse();
                                            Dispose();
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        desktopResponse.SetError(errorCode, "malformed uwp url (package name not found) : " + applicationData);
                                    }
                                }
                                else
                                {
                                    desktopResponse.SetError(errorCode, "malformed uwp url (missing window name) : " + applicationData);
                                }
                            }
                            else
                            {
                                desktopResponse.SetError(errorCode, "malformed uwp url (sould be 'uwp://[UwpApplicationName]/[window name])' : " + applicationData);
                            }
                            SendDesktopResponse();
                            Dispose();
                            return false;
                        }
                        else if (applicationProtocol.Equals(HANDLE_PROTOCOLE))
                        {
                            try
                            {
                                desktop.GetWindowByHandle(applicationData);
                                desktopResponse.SetWindow(desktop.GetWindowByHandle(applicationData));
                                SendDesktopResponse();
                                Dispose();
                                return true;
                            }
                            catch (Exception)
                            {
                                desktopResponse.SetError(errorCode, "unable to find window with handle : " + applicationData);
                                SendDesktopResponse();
                                Dispose();
                                return false;
                            }
                        }
                        else if (applicationProtocol.Equals(WINDOW_PROTOCOLE))
                        {
                            try
                            {
                                desktopResponse.SetWindow(desktop.GetWindowByName(applicationData));
                                SendDesktopResponse();
                                Dispose();
                                return true;
                            }
                            catch (Exception)
                            {
                                desktopResponse.SetError(errorCode, "unable to find window with title like : " + applicationData);
                                SendDesktopResponse();
                                Dispose();
                                return false;
                            }
                        }
                        else if (applicationProtocol.Equals(PROCESS_PROTOCOLE) || applicationProtocol.Equals(PROC_PROTOCOLE))
                        {
                            Process appProcess = null;
                            int maxTry = 40;
                            while (appProcess == null && maxTry > 0)
                            {
                                System.Threading.Thread.Sleep(500);
                                appProcess = GetProcessByInfo(applicationData);
                                maxTry--;
                            }

                            if (appProcess != null)
                            {
                                try
                                {
                                    desktopResponse.SetWindow(desktop.GetWindowByProcess(appProcess));
                                    SendDesktopResponse();
                                    Dispose();
                                    return true;
                                }
                                catch (Exception)
                                {
                                    desktopResponse.SetError(errorCode, "unable to find window with process : " + appProcess.ProcessName + " (" + appProcess.Id + ")");
                                    SendDesktopResponse();
                                    Dispose();
                                    return false;
                                }
                            }
                            else
                            {
                                desktopResponse.SetError(errorCode, "unable to find process matching : " + applicationData);
                                SendDesktopResponse();
                                Dispose();
                                return false;
                            }
                        }
                        else
                        {
                            applicationData = Uri.UnescapeDataString(Regex.Replace(applicationData, @"^/", ""));
                            bool powershellScript = false;
                            bool batScript = applicationData.ToLower().EndsWith(".bat") || applicationData.ToLower().EndsWith(".cmd");

                            Process proc = null;

                            if (attach)
                            {
                                proc = GetProcessByFilename(applicationData);
                            }

                            if (proc == null)
                            {
                                if (File.Exists(applicationData))
                                {
                                    string commandArgs = "";
                                    if (args.Length > 1)
                                    {
                                        int newLen = args.Length - 1;
                                        string[] arguments = new string[newLen];
                                        Array.Copy(args, 1, arguments, 0, newLen);
                                        commandArgs = String.Join(" ", arguments);
                                    }

                                    ProcessStartInfo startInfo = null;

                                    if (batScript)
                                    {
                                        startInfo = new ProcessStartInfo("cmd.exe");
                                        startInfo.Arguments = "/k " + applicationData;
                                        if (commandArgs.Length > 0)
                                        {
                                            startInfo.Arguments += " " + commandArgs;
                                        }
                                    }
                                    else
                                    {
                                        startInfo = new ProcessStartInfo();
                                        if (applicationData.ToLower().EndsWith(".ps1"))
                                        {
                                            powershellScript = true;

                                            startInfo.UseShellExecute = true;
                                            startInfo.FileName = @"powershell.exe";
                                            startInfo.Arguments = string.Format("-ExecutionPolicy ByPass -File \"{0}\"", applicationData);
                                        }
                                        else
                                        {
                                            startInfo.FileName = applicationData.Replace("/", @"\");
                                            startInfo.Arguments = commandArgs;
                                        }
                                    }

                                    startInfo.WorkingDirectory = Directory.GetParent(applicationData).FullName;

                                    try
                                    {
                                        Application app = Application.Launch(startInfo);
                                        WaitWindowReady(app);

                                        if (app.HasExited)
                                        {
                                            desktopResponse.SetError(errorCode, "the process has exited, you may try another way to start this application (UWP ?)");
                                            SendDesktopResponse();
                                            Dispose();
                                            return false;
                                        }
                                        else
                                        {
                                            proc = Process.GetProcessById(app.ProcessId);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        desktopResponse.SetError(errorCode, "cannot start application : " + e.ToString() + " - " + e.Message);
                                        SendDesktopResponse();
                                        Dispose();
                                        return false;
                                    }
                                }
                                else
                                {
                                    desktopResponse.SetError(errorCode, "exec file not found : " + applicationData);
                                    SendDesktopResponse();
                                    Dispose();
                                    return false;
                                }
                            }

                            if (proc != null)
                            {
                                DesktopWindow window = null;

                                if (batScript)
                                {
                                    window = desktop.GetConsoleWindow(proc);
                                }
                                else if (powershellScript)
                                {
                                    window = desktop.GetPowerShellWindow(proc);
                                }
                                else
                                {
                                    window = desktop.GetAppMainWindow(proc);
                                }

                                if (window != null)
                                {
                                    window.UpdateApplicationData(proc);
                                    SendDesktopResponse(window);
                                    Dispose();
                                    return true;
                                }
                                else
                                {
                                    desktopResponse.SetError(errorCode, "unable to find window for application : " + applicationData);
                                }
                            }
                        }
                    }
                    else
                    {
                        desktopResponse.SetError(errorCode, "malformed application url [" + appName + "]");
                    }
                }
                else
                {
                    desktopResponse.SetError(errorCode, "no application path data");
                }
            }
            Dispose();
            SendDesktopResponse();
            return false;
        }
    }
}
