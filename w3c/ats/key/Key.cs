﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;

namespace windowsdriver.w3c.ats.key
{
    public abstract class Key : JsonResponse
    {
        protected const int errorCode = -6;
        protected ActionKeyboard action;
        protected Boolean keyDown = false;

        public enum KeyType
        {
            Clear = 0,
            Enter = 1,
            Down = 2,
            Release = 3
        };

        public static readonly Dictionary<string, int> keyTypeDict = new Dictionary<string, int>()
        {
            {"clear", 0},
            {"enter", 1},
            {"down", 2},
            {"release", 3}
        };

        public static KeyType? getEnumKeyType(string type)
        {
            type = type.ToLower();
            KeyType? keyType = null;

            foreach (var pair in keyTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    keyType = (KeyType)pair.Value;
                    return keyType;
                }
            }
            return keyType;
        }
        public Key(HttpListenerContext context, ActionKeyboard action, bool keyDown) : base(context)
        {
            this.action = action;
            this.keyDown = keyDown;
            //this.desktopResponse = new DesktopResponse(context);
        }
        public virtual Boolean Execute()
        {
            return base.SetJsonValue();
        }

        public override void Dispose()
        {
            //action = null;
            base.Dispose();
        }
    }
}