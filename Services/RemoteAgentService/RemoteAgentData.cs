﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Threading;

namespace windowsdriver.Services.RemoteAgentService
{
    /// <summary>
    /// Represents the data and state for a remote agent.
    /// This class encapsulates key properties and functionalities 
    /// such as lock status, last ping time, and timeout management.   
    /// </summary>
    public class RemoteAgentData
    {
        /// <summary>
        /// Indicates whether the remote agent is locked. 
        /// True if locked, false otherwise.
        /// </summary>
        public bool Locked { get; set; } = false;

        /// <summary>
        /// Stores the identifier of who has locked the remote agent. 
        /// It's an empty string if no one has locked it.
        /// </summary>
        public string LockedBy { get; set; } = string.Empty;

        /// <summary>
        /// Records the last time the remote agent sent a ping.
        /// Used to monitor agent's activity and health.
        /// </summary>
        public DateTime LastPingTime { get; set; }

        /// <summary>
        /// A timer to manage timeouts for the remote agent. 
        /// Triggers events when the agent fails to communicate within expected time frames.
        /// </summary>
        public Timer TimeoutTimer { get; set; }

        /// <summary>
        /// Defines the timer interval in seconds.
        /// </summary>
        public int TimerInterval { get; set; } = 30;

        /// <summary>
        /// Defines the timeout duration in seconds.
        /// </summary>
        public readonly int timeout = 1200;

        /// <summary>
        /// Constructor for RemoteAgentData.
        /// </summary>
        public RemoteAgentData() { }
    }
}
