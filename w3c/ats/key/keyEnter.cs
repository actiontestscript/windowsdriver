﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;
using windowsdriver.items;

namespace windowsdriver.w3c.ats.key
{
     class keyEnterData
    {
        public string id = null;
        public string data = null;
    }
    class keyEnter : Key
    {
        public keyEnter(HttpListenerContext context, ActionKeyboard action, bool keyDown) : base(context, action, keyDown) { }
        private keyEnterData datas = new keyEnterData();

        public override bool Execute()
        {
            if (base.Execute())
            {
              datas = JsonConvert.DeserializeObject<keyEnterData>(jsonValue);
                if (datas.id != null)
                {
                    if (!string.IsNullOrEmpty(datas.id))
                    {
                        AtsElement element = CachedElements.Instance.GetElementById(datas.id);
                        if (element != null)
                        {
                            element.Focus();
                        }
                    }
                    action.SendKeysData(datas.data, keyDown);
                    Send();
                    return true;
                }
                else
                {
                    {
                        SetMsgErrorCode("ats_key_enter_error");
                        //response.SetError(errorCode, "enter text data command error");
                    }
                    Send();
                    return true;
                }
            }
            Send();
            return false;
        }
    }

}