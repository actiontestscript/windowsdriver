﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;

namespace windowsdriver.Services.CommandProcessing
{
    public class AllowedIpsCommand : CommandBase
    {
        public override string CommandType => "allowedips=";

        public override string HelpGlobal => "--allowedIps=LIST\t\tcomma-separated allowed list of remote IP address which are allowed to connect to system driver";
        public override string HelpDetails => "--allowedIps=LIST\t\tcomma-separated allowed list of remote IP address which are allowed to connect to system driver";

        public override void Execute(string command, CommandResult commandResult)
        {
            var ipsData = command.Split('=');
            if (ipsData.Length == 1) return;
            try
            {
                foreach (var ip in ipsData[1].Split(','))
                {
                    commandResult.allowedIps.Add(ip);
                }
            }
            catch (FormatException)
            {
                
                Environment.Exit(-1);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }
        }
    }
}
