﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.ats.key
{
    public class KeyFactory
    {
        public Key Create(Key.KeyType? keyType , HttpListenerContext context, ActionKeyboard action, bool keyDown)
        {
            switch (keyType) {
                case Key.KeyType.Clear:
                    return new KeyClear(context, action, keyDown);
                case Key.KeyType.Enter:
                    return new keyEnter(context, action, keyDown);
                case Key.KeyType.Down:
                   return new KeyDown(context, action, keyDown);
                case Key.KeyType.Release:
                    return new KeyRelease(context, action, keyDown);
                default:
                return new KeyOthers(context, action, keyDown);
            }
        }
    }
}
