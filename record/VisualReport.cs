﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Drawing.Text;
using System.Runtime.Serialization;


[DataContract(Name = "com.ats.recorder.VisualReport")]
public class VisualReport
{

    [DataMember(Name = "author")]
    public string Author;

    [DataMember(Name = "description")]
    public string Description;

    [DataMember(Name = "memo")]
    public string Memo;

    [DataMember(Name = "modifiedAt")]
    public string ModifiedAt;

    [DataMember(Name = "modifiedBy")]
    public string ModifiedBy;

    [DataMember(Name = "started")]
    public string Started;

    [DataMember(Name = "groups")]
    public string Groups;

    [DataMember(Name = "id")]
    public string Id;

    [DataMember(Name = "prerequisite")]
    public string Prerequisite;

    [DataMember(Name = "quality")]
    public int Quality = 1;

    [DataMember(Name = "cpuSpeed")]
    public long CpuSpeed = 0L;

    [DataMember(Name = "totalMemory")]
    public long TotalMemory = 0L;

    [DataMember(Name = "cpuCount")]
    public int CpuCount = 0;

    [DataMember(Name = "osInfo")]
    public string OsInfo;

    [DataMember(Name = "externalId")]
    public string ExternalId;

    [DataMember(Name = "script")]
    public string Script;

    public VisualReport() : base() { }
    public VisualReport(string id, string package, string description, string memo, string author, string groups, string prereq, string externalId, int quality, string started, string modifiedAt, string modifiedBy) : this()
    {
        this.Script = package;
        this.Quality = quality;
        this.Started = started;
        this.CpuSpeed = HardwareInfo.GetCpuSpeedInGHz();
        this.CpuCount = HardwareInfo.GetCpuCount();
        this.TotalMemory = HardwareInfo.GetPhysicalMemory();
        this.OsInfo = HardwareInfo.GetOSInformation();

        if (!string.IsNullOrEmpty(id))
        {
            this.Id = id;
        }

        if (!string.IsNullOrEmpty(author))
        {
            this.Author = author;
        }

        if (!string.IsNullOrEmpty(memo))
        {
            this.Memo = memo;
        }

        if (!string.IsNullOrEmpty(modifiedAt))
        {
            this.ModifiedAt = modifiedAt;
        }

        if (!string.IsNullOrEmpty(modifiedBy))
        {
            this.ModifiedBy = modifiedBy;
        }

        if (!string.IsNullOrEmpty(description))
        {
            this.Description = description;
        }

        if (!string.IsNullOrEmpty(groups))
        {
            this.Groups = groups;
        }

        if (!string.IsNullOrEmpty(prereq))
        {
            this.Prerequisite = prereq;
        }

        if (!string.IsNullOrEmpty(externalId))
        {
            this.ExternalId = externalId;
        }
    }
}