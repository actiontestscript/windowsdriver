﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace windowsdriver.w3c.ats
{
    public class ProfileData
    {

        public string userProfile = null;
        public string browserName = null;
    }

    public class Profile : JsonResponse
    {
        private ProfileData datas = new ProfileData();
        public Profile(HttpListenerContext listener, string method) : base(listener)
        {
            if (POST.Equals(method))
            {
                if (base.SetJsonValue())
                {
                    datas = JsonConvert.DeserializeObject<ProfileData>(jsonValue);
                }

                if (datas.userProfile != null)
                {
                    string folderPath = datas.userProfile;
                    try
                    {
                        string browserName = datas.browserName;
                        string path = folderPath;

                        if (!Path.IsPathRooted(folderPath))
                        {
                            path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "ats", "profiles", browserName, folderPath);
                            if (!Directory.Exists(path))
                            {
                                string localFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                                path = Path.Combine(localFolder, "ats", "profiles", browserName, folderPath);
                            }
                        }

                        try
                        {
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }

                            value = new ProfileValue(path);
                        }
                        catch (Exception e)
                        {
                            SetMsgErrorCode("ats_error_profile_userProfile_directory_not_created", e.Message, folderPath);
                            //value = new Error("Create profile folder error", path, e.Message);
                            //value = 
                        }
                    }
                    catch (Exception e)
                    {
                        SetMsgErrorCode("ats_error_profile_userProfile_directory_not_created", e.Message, folderPath);
//                        value = new Error("Create profile folder error", folderPath, e.Message);
                    }
                }
                else
                {
                    SetMsgErrorCode("ats_error_profile_userProfile_not_found", "'userDir' not sent in post data");
//                    value = new Error("Json data error", "'userDir' not sent in post data", "");
                }
            }
        }
    }

    public class ProfileValue
    {
        public string userDataPath;

        public ProfileValue(string userDataPath)
        {
            this.userDataPath = userDataPath;
        }
    }
}
