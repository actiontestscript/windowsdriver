﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;
using System.Threading;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;

namespace windowsdriver.w3c.ats.element
{
    public class ElementDialogBoxData
    {
        public int pId = -1;
    }
    public class ElementDialogBox : Element
    {
        public ElementDialogBox(HttpListenerContext context, DesktopManager desktop) : base(context, desktop) { }
        private ElementDialogBoxData datas = new ElementDialogBoxData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<ElementDialogBoxData>(jsonValue);

                AtsElement[] elems = new AtsElement[0];

                AutomationElement pane = desktop.GetFirstModalPane();
                if (pane != null)
                {
                    PropertyCondition winCondition = pane.ConditionFactory.ByControlType(FlaUI.Core.Definitions.ControlType.Window);
                    AutomationElement dialog = pane.FindFirstChild(winCondition);
                    int maxTry = 20;
                    while (dialog == null && maxTry > 0)
                    {
                        dialog = pane.FindFirstChild(winCondition);
                        Thread.Sleep(200);
                        maxTry--;
                    }

                    if (dialog != null)
                    {
                        elems = new AtsElement[] { new AtsElement(desktop, dialog, null) };
                        dialog.Focus();
                    }
                }
                else
                {
                    elems = desktop.GetMsgBoxs(datas.pId);
                }

                desktopResponse.Elements = elems;
                elems = null;
                Dispose();

                SendDesktopResponse();
                return true;
            }
            SendDesktopResponse();
            return false;
        }
    }
}