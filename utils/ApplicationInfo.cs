﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Diagnostics;
using static DesktopDriver;
using System.Reflection;
using System.IO;

namespace windowsdriver.utils
{
    public class ApplicationInfo
    {
        private const string IE = "ie";
        private const string CHROME = "chrome";
        private const string BRAVE = "brave";
        private const string FIREFOX = "firefox";
        private const string OPERA = "opera";
        private const string MSEDGE = "msedge";
        private const string SAP = "sap";

        private const string OPERA_SPLASH = "opera_gx_splash";
        private const string OPERA_SPLASH_EXE = OPERA_SPLASH + ".exe";

        private const string FIREFOX_LATEST_VERSION = "0.35.0";
        private const string FIREFOX_VERSION_52 = "0.17.0";

        public string name;
        public string version;

        public string path;
        public string driverName;
        public string remoteDriverName;

        public string driverVersion;

        public bool isActive = false;

        public string logOptions = " --log info";

        public ApplicationInfo(string name, string p = null)
        {
            this.name = name;
            this.driverName = name;

            init(name, p);
        }

        public static string GetBrowserPath(string name)
        {
            try
            {
                switch (name)
                {
                    case CHROME:

                        return FindAppProp.getAppPath(
                            @"HKEY_CLASSES_ROOT\ChromeHTML\shell\open\command",
                            @"Google\Chrome\Application\chrome.exe",
                            name);

                    case FIREFOX:

                        string path = null;
                        Process cmd = null;

                        try
                        {
                            cmd = new Process();
                            cmd.StartInfo.FileName = "firefox.exe";
                            cmd.StartInfo.Arguments = "-headless";
                            cmd.StartInfo.CreateNoWindow = true;
                            cmd.StartInfo.UseShellExecute = true;
                            cmd.Start();

                            if (!cmd.HasExited)
                            {
                                path = cmd.MainModule.FileName.Replace("\\\\", "\\");

                            }
                        }
                        catch { }
                        finally
                        {
                            if (cmd != null)
                            {
                                cmd.Kill();
                            }
                        }

                        if (path == null)
                        {
                            path = FindAppProp.getAppPath(@"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe", @"Mozilla Firefox\firefox.exe", name);
                        }

                        return path;

                    case OPERA:

                        string operaExeFile = @"C:\Program Files\Opera\opera.exe";
                        string operaLauncher = "opera";

                        if (!File.Exists(operaExeFile))
                        {
                            operaExeFile = Path.Combine(new string[] { Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Programs", "Opera", "opera.exe" });

                            if (!File.Exists(operaExeFile))
                            {
                                operaExeFile = @"C:\Program Files\Opera\Launcher.exe";
                                operaLauncher = "Launcher";
                            }
                        }

                        if (File.Exists(operaExeFile))
                        {
                            try
                            {
                                DirectoryInfo gparent = Directory.GetParent(operaExeFile);
                                string[] files = Directory.GetFiles(gparent.FullName, OPERA_SPLASH_EXE, SearchOption.AllDirectories);
                                foreach (string file in files)
                                {
                                    string newFileName = file.Replace(OPERA_SPLASH, OPERA_SPLASH + "-DISABLED");
                                    File.Move(file, newFileName);
                                }
                            }
                            catch (Exception) { }
                        }

                        if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("DOCKER")))
                        {
                            return operaExeFile;
                        }

                        return FindAppProp.getAppPath(
                            @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\opera.exe",
                            @"Programs\Opera\" + operaLauncher + ".exe",
                            name);

                    case MSEDGE:

                        return FindAppProp.getAppPath(
                            @"HKEY_CLASSES_ROOT\MSEdgeHTM\shell\open\command",
                            @"Microsoft\Edge\Application\msedge.exe",
                            name);

                    case BRAVE:

                        return FindAppProp.getAppPath(
                            @"HKEY_CLASSES_ROOT\BraveHTML\shell\open\command",
                            @"BraveSoftware\Brave-Browser\Application\brave.exe",
                            name);
                }
            }catch { }

            return "";
        }


        private void init(string name, string p = null)
        {
            driverVersion = null;

            if (name == SAP)
            {
                isActive = true;

                path = "";
                driverVersion = "";

                Assembly assembly = typeof(DesktopDriver).Assembly;
                version = (assembly.GetCustomAttributes(typeof(AssemblySapVersion), false)[0] as AssemblySapVersion).Value;
            }
            else if (p != null)
            {
                SetPath(p);
                if (name == FIREFOX)
                {
                    driverName = "gecko";
                    checkFirefoxVersion();
                }else if(name == IE)
                {
                    driverName = IE;
                    version = "11";
                    driverVersion = "4.14.0.0";
                    isActive = true;
                }
            }
            else
            {
                int offsetVersion = 0;

                path = GetBrowserPath(name);

                if(name == FIREFOX)
                {
                    version = FindAppProp.getAppVersion(path);
                    driverName = "gecko";
                    checkFirefoxVersion();
                }
                else if (name == OPERA)
                {
                    offsetVersion = 14;
                    version = FindAppProp.getAppVersion(path);
                }
                else
                {
                    version = FindAppProp.getAppVersion(path);
                }
                

                if (version == null)
                {
                    isActive = false;
                }
                else
                {
                    if (driverVersion == null)
                    {
                        driverVersion = (GetDriverMajorVersion(version, offsetVersion)).ToString();
                    }

                    isActive = true;
                }
            }

            remoteDriverName = driverName + "driver";
        }

        private void checkFirefoxVersion()
        {
            if (version != null && version.StartsWith("52."))
            {
                driverVersion = FIREFOX_VERSION_52;
            }
            else
            {
                driverVersion = FIREFOX_LATEST_VERSION;
                logOptions += " --log-no-truncate";
            }
        }

        public bool UpdatePath(string p)
        {
            string currentPath = path;

            init(name, p);

            return currentPath != path;
        }

        private void SetPath(string p)
        {
            path = p;
            version = FindAppProp.getAppVersion(path);
            if (version != null)
            {
                driverVersion = GetDriverMajorVersion(version, name == OPERA ? 14 : 0).ToString();
                isActive = true;
            }
        }

        public bool matchDriver(string v)
        {
            if(name == FIREFOX)
            {
                if (version != null && version.StartsWith("52."))
                {
                    return driverVersion == FIREFOX_VERSION_52;
                }
                else
                {
                    return driverVersion == FIREFOX_LATEST_VERSION;
                }
            }
            else
            {
                return driverVersion == GetDriverMajorVersion(v).ToString();
            }
        }

        public static int GetDriverMajorVersion(string version, int offset = 0)
        {
            try
            {
                return int.Parse(version.Split('.')[0]) + offset;
            }
            catch { }

            return 0;
        }
    }
}