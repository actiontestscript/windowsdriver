﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;

namespace windowsdriver.w3c.ats.recorder
{
    class DriverInfo
    {
        public Boolean headless=false;
        public String sessionId="";
        public String screenshotUrl="";
    }

    public abstract class Recorder : JsonResponse
    {
        public enum RecordType
        {
            Stop = 0,
            Screenshot = 1,
            Start = 2,
            Create = 3,
            Image = 4,
            Value = 5,
            Data = 6,
            Status = 7,
            Element = 8,
            Position = 9,
            Download = 10,
            ImageMobile = 11,
            CreateMobile = 12,
            ScreenshotMobile = 13,
            Summary = 14,
        };

        public static readonly Dictionary<string, int> driverTypeDict = new Dictionary<string, int>()
    {
        {"stop", 0},
        {"screenshot", 1},
        {"start", 2},
        {"create", 3},
        {"image", 4},
        {"value", 5},
        {"data", 6},
        {"status", 7},
        {"element", 8},
        {"position", 9},
        {"download", 10},
        {"imagemobile", 11},
        {"createmobile", 12},
        {"screenshotmobile", 13},
        {"summary", 14},
    };

        public VisualRecorder recorder;
        protected DesktopResponse desktopResponse;

        public static RecordType? getEnumRecordType(string type)
        {
            type = type.ToLower();
            RecordType? recordType = null;
            foreach (var pair in driverTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    recordType = (RecordType)pair.Value;
                    break;
                }
            }
            return recordType;
        }

        public Recorder(HttpListenerContext context, VisualRecorder recorder) : base(context) 
        {
            this.recorder = recorder;
            this.desktopResponse = new DesktopResponse(context);
            
        }

        public virtual Boolean Execute( )
        {
            return base.SetJsonValue();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        protected void SendDesktopResponse()
        {
            desktopResponse.Send(desktopResponse);
        }
    }
}
