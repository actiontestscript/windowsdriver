﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.AutomationElements;
using FlaUI.Core.Definitions;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using windowsdriver;
using windowsdriver.utils.JsonUtils;


class WindowExecution : AtsExecution
{
    public const string REFRESH = "refresh";
    public const string NEXT = "next";
    public const string BACK = "back";

    //-------------------------------------------------
    // com.ats.executor.ActionStatus -> status code
    //-------------------------------------------------
    private const int UNREACHABLE_GOTO_URL = -11;
    private const int WINDOW_NOT_FOUND = -14;
    private const int CLOSE_WINDOW = -19;
    //-------------------------------------------------

    public enum WindowType
    {
        Title = 0,
        Handle = 1,
        List = 2,
        Move = 3,
        Resize = 4,
        ToFront = 5,
        Switch = 6,
        Close = 7,
        Url = 8,
        Keys = 9,
        State = 10,
        CloseWindow = 11,
        CloseModalWindows = 12
    };


    Dictionary<string, int> windowTypeDict = new Dictionary<string, int>()
    {
        {"title", 0},
        {"handle", 1},
        {"list", 2},
        {"move", 3},
        {"resize", 4},
        {"tofront", 5},
        {"switch", 6},
        {"close", 7},
        {"url", 8},
        {"keys", 9},
        {"state", 10},
        {"closewindow", 11},
        {"closemodalwindows", 12}
    };

    private WindowType? getEnumWindowType(string type)
    {
        type = type.ToLower();

        WindowType windowType;
        foreach(var pair in windowTypeDict)
        {
            if(string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
            {
                windowType = (WindowType) pair.Value;
                return windowType;
            }
        }
        return null;
    }

    private readonly Executor executor;

    #region Constructor
    public WindowExecution(string type, JObject node, ActionKeyboard keyboard, VisualRecorder recorder, DesktopManager desktop): base()
    {
        response.type = DesktopResponseType.W3CJSON;
        executor = WindowExec(type, node, keyboard, recorder, desktop);
    }

    public WindowExecution(WindowType stype, string[] commandsData, ActionKeyboard keyboard, VisualRecorder recorder, DesktopManager desktop) : base()
    {

        executor = WindowExec(stype, commandsData, keyboard, recorder, desktop);
    }

    public WindowExecution(string stype, string postData, ActionKeyboard keyboard, VisualRecorder recorder, DesktopManager desktop) : base()
    {
        response.SetError(500, "Not implemented");
        return;
        /*
        string[] commandsData = new List<string>().ToArray();

        if (windowTypeDict.TryGetValue(stype.ToLower(), out int sTypeVal))
        {
            executor = WindowExec((WindowType)sTypeVal, commandsData, keyboard, recorder, desktop);
        }
        else
        {
            //TODO: Send exception
        }
        */
    }
    #endregion

    //Json format:
    private Executor WindowExec(string type, JObject node, ActionKeyboard keyboard, VisualRecorder recorder, DesktopManager desktop)
    {
        DesktopWindow window = null;
        Executor executor = null;
        WindowType? windowType = getEnumWindowType(type);
        int handle = JsonUtils.GetJsonValue<int>(node, "handle", -1);
        if (windowType == null) { response.SetError(500, "Not implemented"); }
//        else if (type.ToLower().Equals("close") || type.ToLower().Equals("closewindow") || type.ToLower().Equals("handle") || type.ToLower().Equals("state") || type.ToLower().Equals("keys") )
        else if (windowType == WindowType.Close || windowType == WindowType.CloseWindow || windowType == WindowType.Handle || windowType == WindowType.State || windowType == WindowType.Keys)
        {
            window = desktop.GetWindowByHandle(handle);

            if (window != null)
            {
                if (windowType == WindowType.State)
                {
                    string state = JsonUtils.GetJsonValue<string>(node,"state", "" );
                    executor = new StateExecutor(window, response, state);
                }
                else if (windowType == WindowType.Keys)
                {
                    string keys = JsonUtils.GetJsonValue<string>(node, "keys","");
                    executor = new KeysExecutor(window, desktop, response, keyboard, keys);
                }
                else if (windowType == WindowType.Close)
                {
                    executor = new CloseExecutor(window, response);
                }
                else if (windowType == WindowType.Handle)
                {
                    executor = new HandleExecutor(window, response);
                }
                else if (windowType == WindowType.CloseWindow)
                {
                    string errorMessage = window.CloseWindow();
                    if (errorMessage != null)
                    {
                        response.SetError(CLOSE_WINDOW, errorMessage);
                    }
                }
            }
        }
        
        else if (windowType == WindowType.ToFront)
        {
            int pid = JsonUtils.GetJsonValue<int>(node, "pid", -1);

            if (handle != -1)
            {
                window = desktop.GetWindowByHandle(handle);

                if (window == null)
                {
                    window = desktop.GetWindowIndexByPid(pid, 0);
                }

                recorder.CurrentPid = pid;
            }
            else
            {
                recorder.CurrentPid = pid;

                List<DesktopWindow> windows = desktop.GetOrderedWindowsByPid(pid);
                if (windows.Count > 0)
                {
                    window = windows[0];
                }
            }

            if (window != null)
            {
                window.ToFront();
            }
        }
        else if ( windowType == WindowType.Url )
        {
            window = desktop.GetWindowByHandle(handle);
            string value = JsonUtils.GetJsonValue<string>(node, "value", "");
            if (window.isIE)
            {
                window.ToFront();

                AutomationElement win = window.Element;
                AutomationElement addressBar = win.FindFirst(TreeScope.Descendants, win.ConditionFactory.ByControlType(ControlType.Pane).And(win.ConditionFactory.ByClassName("Address Band Root")));
                if (addressBar != null)
                {
                    addressBar.Focus();
                    AutomationElement edit = addressBar.FindFirstChild(addressBar.ConditionFactory.ByControlType(ControlType.Edit));

                    if (edit != null)
                    {
                        try
                        {
                            edit.Focus();
                            edit.Click();
                            if (edit.Patterns.Value.IsSupported)
                            {
                                edit.Patterns.Value.Pattern.SetValue(value);
                            }
                            Keyboard.Type(VirtualKeyShort.ENTER);
                        }
                        catch { }
                    }
                }
            }
            else
            {
                if (REFRESH.Equals(value) || NEXT.Equals(value) || BACK.Equals(value))
                {
                    window.Navigate(value, desktop);
                }
                else
                {

                    string fname = Environment.ExpandEnvironmentVariables(value);
                    if (fname.StartsWith("file:///"))
                    {
                        fname = fname.Substring(8);
                    }

                    try
                    {
                        FileAttributes attr = File.GetAttributes(@fname);
                        if (!attr.HasFlag(FileAttributes.Directory))
                        {
                            DirectoryInfo dirInfo = Directory.GetParent(@fname);
                            fname = dirInfo.FullName;
                        }

                        if (Directory.Exists(@fname))
                        {
                            window.GotoUrl(Path.GetFullPath(@fname), desktop);
                        }
                        else
                        {
                            response.SetError(UNREACHABLE_GOTO_URL, "directory not found : " + fname);
                        }
                    }
                    catch (Exception e)
                    {
                        response.SetError(UNREACHABLE_GOTO_URL, "directory path not valid : " + fname + " (" + e.Message + ")");
                    }
                }
            }
        }
        else if (windowType == WindowType.Title )
        {
            string title = JsonUtils.GetJsonValue<string>(node, "title","");
            string name = JsonUtils.GetJsonValue<string>(node, "name","");
            if (title.Equals("jx"))
            {
                window = desktop.GetJxWindowPid(name);
            }
            else
            {
                window = desktop.GetWindowByTitle(title);
            }
        }
        else if (windowType == WindowType.List )
        {
            int pid = JsonUtils.GetJsonValue<int>(node, "pid", -1);
            executor = new ListExecutor(window, response, desktop.GetOrderedWindowsByPid(pid).ToArray());
        }
        else if (windowType == WindowType.Switch )
        {
            int pid = JsonUtils.GetJsonValue<int>(node, "pid", -1);
            int index = JsonUtils.GetJsonValue<int>(node, "index", -1);
            if (pid != -1 && index != -1)
            {

                window = desktop.GetWindowIndexByPid(pid, index);

                if (window == null)
                {
                    window = desktop.GetWindowByHandle(handle);
                }
            }
            else
            {
                window = desktop.GetWindowByHandle(handle);
            }

            if (window != null)
            {
                window.ToFront();
            }
        }
        else if ( windowType == WindowType.Move || windowType == WindowType.Resize )
        {
            int value1 = JsonUtils.GetJsonValue<int>(node, "value1", -1);
            int value2 = JsonUtils.GetJsonValue<int>(node, "value2", -1);

            window = desktop.GetWindowByHandle(handle);
            if (window != null)
            {
                if ( windowType == WindowType.Move )
                {
                    executor = new MoveExecutor(window, response, value1, value2);
                }
                else
                {
                    executor = new ResizeExecutor(window, response, value1, value2);
                }
            }
        }
        else if ( windowType == WindowType.CloseModalWindows )
        {
            int pid = JsonUtils.GetJsonValue<int>(node, "pid", -1);
            List<DesktopWindow> windows = desktop.GetOrderedWindowsByPid(pid);

            foreach (DesktopWindow win in windows)
            {
                window = win;
                window.CloseModalWindows();
            }
        }


        if (executor == null)
        {
            executor = new EmptyExecutor(window, response);
        }

        return executor;
    }

    #region WindowExec
    private Executor WindowExec(WindowType type, string[] commandsData, ActionKeyboard keyboard, VisualRecorder recorder, DesktopManager desktop)
    {
        DesktopWindow window = null;
        Executor executor = null;

        if (type == WindowType.Close || type == WindowType.CloseWindow || type == WindowType.Handle || type == WindowType.State || type == WindowType.Keys)
        {
            _ = int.TryParse(commandsData[0], out int handle);
            window = desktop.GetWindowByHandle(handle);
            if (window != null)
            {
                if (type == WindowType.State)
                {
                    executor = new StateExecutor(window, response, commandsData[1]);
                }
                else if (type == WindowType.Keys)
                {
                    executor = new KeysExecutor(window, desktop, response, keyboard, commandsData[1]);
                }
                else if (type == WindowType.Close)
                {
                    executor = new CloseExecutor(window, response);
                }
                else if (type == WindowType.Handle)
                {
                    executor = new HandleExecutor(window, response);
                }
                else if (type == WindowType.CloseWindow)
                {
                    string errorMessage = window.CloseWindow();
                    if (errorMessage != null)
                    {
                        response.SetError(CLOSE_WINDOW, errorMessage);
                    }
                }
            }
        }
        else if (type == WindowType.ToFront)
        {
            if (commandsData.Length > 1)
            {
                _ = int.TryParse(commandsData[0], out int handle);
                _ = int.TryParse(commandsData[1], out int pid);

                window = desktop.GetWindowByHandle(handle);

                if (window == null)
                {
                    window = desktop.GetWindowIndexByPid(pid, 0);
                }

                recorder.CurrentPid = pid;
            }
            else
            {
                _ = int.TryParse(commandsData[0], out int pid);
                recorder.CurrentPid = pid;

                List<DesktopWindow> windows = desktop.GetOrderedWindowsByPid(pid);
                if (windows.Count > 0)
                {
                    window = windows[0];
                }
            }

            if (window != null)
            {
                window.ToFront();
            }
        }
        else if (type == WindowType.Url)
        {
            _ = int.TryParse(commandsData[0], out int handle);
            window = desktop.GetWindowByHandle(handle);

            if (window.isIE)
            {
                window.ToFront();

                AutomationElement win = window.Element;
                AutomationElement addressBar = win.FindFirst(TreeScope.Descendants, win.ConditionFactory.ByControlType(ControlType.Pane).And(win.ConditionFactory.ByClassName("Address Band Root")));
                if (addressBar != null)
                {
                    addressBar.Focus();
                    AutomationElement edit = addressBar.FindFirstChild(addressBar.ConditionFactory.ByControlType(ControlType.Edit));

                    if (edit != null)
                    {
                        try
                        {
                            edit.Focus();
                            edit.Click();
                            if (edit.Patterns.Value.IsSupported)
                            {
                                edit.Patterns.Value.Pattern.SetValue(commandsData[1]);
                            }
                            Keyboard.Type(VirtualKeyShort.ENTER);
                        }
                        catch { }
                    }
                }
            }
            else
            {
                if (REFRESH.Equals(commandsData[1]) || NEXT.Equals(commandsData[1]) || BACK.Equals(commandsData[1]))
                {
                    window.Navigate(commandsData[1], desktop);
                }
                else
                {

                    string fname = Environment.ExpandEnvironmentVariables(commandsData[1]);
                    if (fname.StartsWith("file:///"))
                    {
                        fname = fname.Substring(8);
                    }

                    try
                    {
                        FileAttributes attr = File.GetAttributes(@fname);
                        if (!attr.HasFlag(FileAttributes.Directory))
                        {
                            DirectoryInfo dirInfo = Directory.GetParent(@fname);
                            fname = dirInfo.FullName;
                        }

                        if (Directory.Exists(@fname))
                        {
                            window.GotoUrl(Path.GetFullPath(@fname), desktop);
                        }
                        else
                        {
                            response.SetError(UNREACHABLE_GOTO_URL, "directory not found : " + fname);
                        }
                    }
                    catch (Exception e)
                    {
                        response.SetError(UNREACHABLE_GOTO_URL, "directory path not valid : " + fname + " (" + e.Message + ")");
                    }
                }
            }
        }
        else if (type == WindowType.Title)
        {
            if (commandsData[0].Equals("jx"))
            {
                window = desktop.GetJxWindowPid(commandsData[1]);
            }
            else
            {
                window = desktop.GetWindowByTitle(commandsData[0]);
            }
        }
        else if (type == WindowType.List)
        {
            _ = int.TryParse(commandsData[0], out int pid);
            executor = new ListExecutor(window, response, desktop.GetOrderedWindowsByPid(pid).ToArray());
        }
        else if (type == WindowType.Switch)
        {
            if (commandsData.Length > 1)
            {
                _ = int.TryParse(commandsData[0], out int pid);
                _ = int.TryParse(commandsData[1], out int index);

                window = desktop.GetWindowIndexByPid(pid, index);

                if (window == null)
                {
                    _ = int.TryParse(commandsData[2], out int handle);
                    window = desktop.GetWindowByHandle(handle);
                }
            }
            else
            {
                _ = int.TryParse(commandsData[0], out int handle);
                window = desktop.GetWindowByHandle(handle);
            }

            if (window != null)
            {
                window.ToFront();
            }
        }
        else if (type == WindowType.Move || type == WindowType.Resize)
        {
            _ = int.TryParse(commandsData[0], out int handle);
            _ = int.TryParse(commandsData[1], out int value1);
            _ = int.TryParse(commandsData[2], out int value2);

            window = desktop.GetWindowByHandle(handle);
            if (window != null)
            {
                if (type == WindowType.Move)
                {
                    executor = new MoveExecutor(window, response, value1, value2);
                }
                else
                {
                    executor = new ResizeExecutor(window, response, value1, value2);
                }
            }
        }
        else if (type == WindowType.CloseModalWindows)
        {
            _ = int.TryParse(commandsData[0], out int pid);
            List<DesktopWindow> windows = desktop.GetOrderedWindowsByPid(pid);

            foreach (DesktopWindow win in windows)
            {
                window = win;
                window.CloseModalWindows();
            }
        }

        if (executor == null)
        {
            executor = new EmptyExecutor(window, response);
        }

        return executor;
    }
    #endregion

    private abstract class Executor
    {
        protected readonly DesktopWindow window;
        protected readonly DesktopResponse response;

        public Executor(DesktopWindow window, DesktopResponse response)
        {
            this.window = window;
            this.response = response;
        }
        public abstract void Run();
    }

    private class EmptyExecutor : Executor
    {
        public EmptyExecutor(DesktopWindow window, DesktopResponse response) : base(window, response) { }
        public override void Run()
        {
            if (window != null)
            {
                response.SetWindow(window );
            }
            else
            {
                response.SetError(WINDOW_NOT_FOUND, "window not found");
            }
        }
    }

    private class HandleExecutor : Executor
    {
        public HandleExecutor(DesktopWindow window, DesktopResponse response) : base(window, response) { }
        public override void Run()
        {
            response.SetWindow( window );
        }
    }

    private class ListExecutor : Executor
    {
        protected DesktopWindow[] windows;

        public ListExecutor(DesktopWindow window, DesktopResponse response, DesktopWindow[] windows) : base(window, response)
        {
            this.windows = windows;
        }

        public override void Run()
        {
            response.SetWindows( windows );
        }
    }

    private class MoveExecutor : Executor
    {
        protected int x = 0;
        protected int y = 0;

        public MoveExecutor(DesktopWindow window, DesktopResponse response, int value1, int value2) : base(window, response)
        {
            x = value1;
            y = value2;
        }

        public override void Run()
        {
            window.Move(x, y);
        }
    }

    private class ResizeExecutor : Executor
    {
        protected int w = 0;
        protected int h = 0;

        public ResizeExecutor(DesktopWindow window, DesktopResponse response, int value1, int value2) : base(window, response)
        {
            w = value1;
            h = value2;
        }

        public override void Run()
        {
            if (w > 0 && h > 0)
            {
                window.Resize(w, h);
            }
        }
    }

    private class StateExecutor : Executor
    {
        protected string state;

        public StateExecutor(DesktopWindow window, DesktopResponse response, string state) : base(window, response)
        {
            this.state = state;
        }

        public override void Run()
        {
            window.ChangeState(state);
            Dispose();
        }

        public void Dispose()
        {
            state = null;
        }
    }
    private class CloseExecutor : Executor
    {
        public CloseExecutor(DesktopWindow window, DesktopResponse response) : base(window, response) { }
        public override void Run()
        {
            window.Close();
        }
    }

    private class KeysExecutor : Executor
    {
        protected ActionKeyboard keyboard;
        protected string keys;
        protected DesktopManager desktop;

        public KeysExecutor(DesktopWindow window, DesktopManager desktop, DesktopResponse response, ActionKeyboard keyboard, string keys) : base(window, response)
        {
            this.desktop = desktop;
            this.keyboard = keyboard;
            this.keys = keys;
        }

        public override void Run()
        {
            if (window != null)
            {
                window.SetMouseFocus(desktop);
                Thread.Sleep(200);
            }
            keyboard.RootKeys(keys);

            Dispose();
        }

        public void Dispose()
        {
            keyboard = null;
            keys = null;
        }
    }

    public override bool Run(HttpListenerContext context)
    {
        executor.Run();
        return base.Run(context);
    }
}