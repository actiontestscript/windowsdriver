﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using windowsdriver.utils;

namespace windowsdriver.w3c.ats
{
    public class WindowTitleData
    {

        public string title = null;
        public string name = null;
    }
    public class WindowTitle : JsonResponse
    {

        private WindowTitleData datas = new WindowTitleData();

        public WindowTitle(HttpListenerContext listener, string method, Capabilities caps, List<string> urlData, DesktopManager desktop) : base(listener)
        {
            if (DELETE.Equals(method))
            {

            }
            else if (GET.Equals(method))
            {
                value = "";
                if (urlData.Count > 2)
                {
                    string titleId = urlData[2];

                    int maxTry = 20;
                    DesktopWindow window = null;

                    while (window == null && maxTry > 0)
                    {
                        Thread.Sleep(100);
                        window = desktop.GetWindowByName(titleId);
                        maxTry--;
                    }

                    value = window;

                    /*RemoteDriver rd = caps.GetRemoteDriverById(id);
                    if(rd != null)
                    {
                        int processId = rd.GetProcessId();
                        if(processId > 0)
                        {
                            int maxTry = 20;
                            DesktopWindow window = null;

                            while(window == null && maxTry > 0)
                            {
                                Thread.Sleep(250);
                                window = desktop.GetFirstWindowsByPid(processId);
                                maxTry--;
                            }

                            if(window != null)
                            {
                                value += window.Handle.ToString();
                            }
                        }
                    }*/
                }
            }
            else if (POST.Equals(method))
            {
                if (base.SetJsonValue())
                {
                    datas = JsonConvert.DeserializeObject<WindowTitleData>(jsonValue);
                }

                value = null;

                if(datas.title != null)
                {
                    string title = datas.title;
                    value = desktop.GetWindowByName(title);
                }

                /*
                if (urlData.Count > 1 && urlData[1] == "title")
                {
                    string title = jsonPostedData.Value?.title;
                    value = desktop.GetWindowByName(title);
                }
                */
            }
        }
    }
}