﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.AutomationElements;
using FlaUI.Core.Definitions;
using FlaUI.Core.EventHandlers;
using System.Collections.Generic;

namespace windowsdriver.desktop
{
    struct WindowHandle
    {
        //private static readonly int WinCloseEventId = System.Windows.Automation.WindowPattern.WindowClosedEvent.Id;
        private const int WinCloseEventId = 20017;

        public readonly int Handle;
        private readonly int Pid;
        private readonly ControlType type;

        public AutomationElement Win;
        private AutomationEventHandlerBase CloseEvent;
        private StructureChangedEventHandlerBase StructureChangedEvent;

        public WindowHandle(DesktopManager desktop, int pid, int handle, AutomationElement win)
        {
            Pid = pid;
            Win = win;
            Handle = handle;

            try
            {
                type = win.ControlType;
            }
            catch
            {
                type = ControlType.Unknown;
            }

            CloseEvent = Win.RegisterAutomationEvent(new FlaUI.Core.Identifiers.EventId(WinCloseEventId, "WindowClosedEvent"), TreeScope.Element, (element, type) => desktop.HandleRemove(handle, false));
            StructureChangedEvent = Win.RegisterStructureChangedEvent(TreeScope.Children, (element, type, arg) => desktop.AddPopup(pid, element));
        }

        public bool HasNoParent()
        {
            return Win.Parent == null;
        }

        public bool IsDocument()
        {
            return type == ControlType.Document;
        }

        public void Update(AutomationElement win)
        {
            Win = win;
        }

        public void Reduce()
        {
            try
            {
                Win.Patterns.Window.Pattern.SetWindowVisualState(WindowVisualState.Minimized);
            }
            catch { }
        }

        public void Dispose()
        {
            try
            {
                CloseEvent.Dispose();
                StructureChangedEvent.Dispose();
            }
            catch { }

            CloseEvent = null;
            StructureChangedEvent = null;
            Win = null;
        }

        public bool CheckPid(List<int> pids)
        {
            return pids.IndexOf(Pid) > -1;
        }

        public string GetLogs()
        {
            string data = "Name = " + Win.Name + "\n";
            data += "Id = " + Win.Properties.ProcessId + "\n";
            data += "Handle = " + Win.Properties.NativeWindowHandle + "\n";
            data += "Rectangle = " + Win.Properties.BoundingRectangle.ToString() + "\n";

            return data;
        }
    }
}