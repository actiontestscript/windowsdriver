﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using windowsdriver.utils;
using windowsdriver.Utils.Logger;

namespace windowsdriver.Services.CommandProcessing
{
    public class AllWebDriver : CommandBase
    {
        public override string CommandType => "allwebdriver";
        public override string HelpGlobal => "--allWebDriver=true\t\tdownload all Web Driver";
        public override string HelpDetails => "--allWebDriver=true\t\tdownload all Web Driver";
//        public override string HelpDetails => "  --allWebDriver Download all Web Driver\n   Usage : --allWebDriver";

        public override void Execute(string command, CommandResult commandResult)
        {
            var allWeb = command.Split('=');
            var length = allWeb.Length;
            if (allWeb.Length == 1 || allWeb[1].ToLower().Equals("false")) return;

            AtsLogger.Init(false);
            
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();
            string appPath = null;
            string[] webBrowsers = { "chrome", "firefox", "msedge", "brave", "opera" };
            foreach (var webBrowser in webBrowsers)
            {
                
                RemoteDriver remoteDriver = new RemoteDriver(currentDirectory, webBrowser, appPath, commandResult);
                remoteDriver.Dispose();
                
            }
            Environment.Exit(0);
        }
    }   
}
