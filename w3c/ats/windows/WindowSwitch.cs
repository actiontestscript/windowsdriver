﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System.Net;

namespace windowsdriver.w3c.ats.windows
{
    public class WindowSwitchData
    {
        public int handle = -1;
        public int pid = -1;
        public int index = -1;
    }

    public class WindowSwitch : Window
    {
        public WindowSwitch(HttpListenerContext context, ActionKeyboard keyboard, DesktopManager desktop, VisualRecorder recorder) : base(context, keyboard, desktop, recorder) { }
        private WindowSwitchData datas = new WindowSwitchData();

        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<WindowSwitchData>(jsonValue);
                if (datas.pid != -1 && datas.index != -1)
                {
                    window = desktop.GetWindowIndexByPid(datas.pid, datas.index);
                    if(window == null)
                    {
                        window = desktop.GetWindowByHandle(datas.handle);
                    }
                }
                else
                {
                    window = desktop.GetWindowByHandle(datas.handle);

                }
                if (window != null)
                {
                    window.ToFront();
                }
                desktopResponse.SetWindow( window );
                SendDesktopResponse(window);
                return true;
            }
            SendDesktopResponse();
            return false;
        }
    }
}