﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using DotAmf.Serialization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using windowsdriver;
using windowsdriver.w3c.ats.recorder;
using windowsdriver.record;
using System.Reflection;
using windowsdriver.Utils.Logger;

public class VisualRecorder
{
    [DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
    private static extern IntPtr GetDesktopWindow();

    [DllImport("user32.dll", EntryPoint = "GetDC")]
    private static extern IntPtr GetDC(IntPtr ptr);

    [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC")]
    private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

    [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
    private static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);

    [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
    private static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

    [DllImport("gdi32.dll", EntryPoint = "BitBlt")]
    private static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, int RasterOp);

    [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
    private static extern IntPtr DeleteObject(IntPtr hDc);

    [DllImport("gdi32.dll")]
    private static extern bool DeleteDC(IntPtr hDC);

    private const int SRCCOPY = 0x00CC0020;

    private const string API_SESSION = "api-session";

    private int frameIndex = 0;
    private BufferedStream visualStream;
    private FileStream fileVisualStream;
    private IVisualAction currentAction = new VisualActionNull();

    //private ImageCodecInfo animationEncoder;
    //private EncoderParameters animationEncoderParameters;

    public static string API_TYPE = "api";

    private string imageType = "webp";
    private static float imageQuality = 25F;

    private DataContractAmfSerializer AmfSerializer;

    private TestSummary summary;

    private string AtsvFilePath;
    public string AmfTempFilePath = "";

    /*private PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
    private PerformanceCounter ramCounter;
    private PerformanceCounter networkBytesSent = new PerformanceCounter("Network Interface", "Bytes Sent/sec", true);
    private PerformanceCounter networkBytesReceived = new PerformanceCounter("Network Interface", "Bytes Received/sec", true);*/

    private int currentPid = -1;

    //private static readonly ImageCodecInfo PNG_ENCODER;
    //private static readonly EncoderParameters PNG_ENCODER_PARAMETER;

    /*static VisualRecorder()
    {
        //PNG_ENCODER = GetEncoder(ImageFormat.Png);
        //PNG_ENCODER_PARAMETER = new EncoderParameters(1);
        //PNG_ENCODER_PARAMETER.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
    }*/

    public int CurrentPid
    {
        get { return currentPid; }
        set
        {
            if (value != currentPid)
            {
                currentPid = value;
            }
        }
    }

    /*public static ImageCodecInfo GetEncoder(ImageFormat format)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
        foreach (ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }*/

    public Bitmap ScreenCapture(double[] bound, string url)
    {
       return ScreenCapture(bound, VisualAction.GetScreenshotImage(url));
    }

    public Bitmap ScreenCapture(double[] bound)
    {
        return ScreenCapture((int)bound[0], (int)bound[1], (int)bound[2], (int)bound[3]);
    }

    /*public byte[] ScreenCapture(TestBound bounds)
    {
        return ScreenCapture((int)bounds.X, (int)bounds.Y, (int)bounds.Width, (int)bounds.Height);
    }*/

    public Bitmap ScreenCapture(double[] bound, Bitmap img)
    {
        return ScreenCapture((int)bound[0], (int)bound[1], (int)bound[2], (int)bound[3], img);
    }

    public static Bitmap ScreenCapture(int x, int y, int w, int h)
    {
        IntPtr hdcSrc = GetDC(GetDesktopWindow());
        IntPtr hdcDest = CreateCompatibleDC(hdcSrc);
        IntPtr hBitmap = CreateCompatibleBitmap(hdcSrc, w, h);

        if (hBitmap != IntPtr.Zero)
        {
            IntPtr hOld = (IntPtr)SelectObject(hdcDest, hBitmap);

            BitBlt(hdcDest, 0, 0, w, h, hdcSrc, x, y, SRCCOPY);
            SelectObject(hdcDest, hOld);

            DeleteDC(hdcSrc);
            DeleteDC(hdcDest);

            //return Image.FromHbitmap(hBitmap);

            Bitmap bitmap = Image.FromHbitmap(hBitmap);
            DeleteObject(hBitmap);
            DeleteObject(hOld);

            //GC.Collect();

            return bitmap;

            /*using (bitmap = Image.FromHbitmap(hBitmap))
            {
                DeleteObject(hBitmap);
                GC.Collect();

                return bitmap;
            }*/
        }
        return null;
    }

    public static Bitmap ScreenCapture(int x, int y, int w, int h, Bitmap img)
    {
        Bitmap clone = img.Clone(new Rectangle(x, y, w, h), img.PixelFormat);
        img.Dispose();
        return clone;
    }

    internal void Stop()
    {
        Flush();

        if (IsActivated())
        {
            //visualStream.Flush();

            AmfSerializer.WriteObject(visualStream, summary);

            visualStream.Flush();
            visualStream.Close();
        }

        visualStream = null;
        AmfSerializer = null;
    }

    internal void Start(string folderPath, string id, string fullName, string description, string memo, string author, string groups, string prereq, string externalId, int videoQuality, string started, string modifiedAt, string modifiedBy)
    {
        currentAction = new VisualAction(fullName);
        visualStream = null;
        AmfSerializer = null;

        frameIndex = -1;

        if (videoQuality > 0)
        {
            if (AmfSerializer == null)
            {
                AmfSerializer = new DataContractAmfSerializer(typeof(VisualReport), new[] { typeof(TestSummary), typeof(TestError), typeof(VisualAction), typeof(VisualElement), typeof(TestBound) });
            }

            /*if (videoQuality == 4) // max quality level
            {
                imageType = "png";
                animationEncoder = PNG_ENCODER;
                animationEncoderParameters = PNG_ENCODER_PARAMETER;
            }
            else
            {
                animationEncoder = GetEncoder(ImageFormat.Jpeg);
                animationEncoderParameters = new EncoderParameters(2);

                if (videoQuality == 3) // quality level
                {
                    animationEncoderParameters.Param[1] = new EncoderParameter(Encoder.Quality, 70L);
                    animationEncoderParameters.Param[0] = new EncoderParameter(Encoder.Compression, 70L);
                }
                else if (videoQuality == 2)// speed level
                {
                    animationEncoderParameters.Param[1] = new EncoderParameter(Encoder.Quality, 35L);
                    animationEncoderParameters.Param[0] = new EncoderParameter(Encoder.Compression, 20L);
                }
                else // size level
                {
                    animationEncoderParameters.Param[1] = new EncoderParameter(Encoder.Quality, 10L);
                    animationEncoderParameters.Param[0] = new EncoderParameter(Encoder.Compression, 100L);
                }
            }*/

            if (!IsActivated())
            {
                AtsvFilePath = folderPath + "\\atsv_" + Guid.NewGuid() + ".tmp";
                try
                {
                    fileVisualStream = new FileStream(AtsvFilePath, FileMode.Create);
                    visualStream = new BufferedStream(fileVisualStream);

                    AmfSerializer.WriteObject(visualStream, new VisualReport(id, fullName, description, memo, author, groups, prereq, externalId, videoQuality, started, modifiedAt, modifiedBy));
                    visualStream.Flush();
                }
                catch { }
            }
        }
    }

    internal void Summary(bool passed, int actions, string suiteName, string testName, string data)
    {
        summary = new TestSummary(passed, actions, suiteName, testName, data);
    }

    internal void Summary(bool passed, int actions, string suiteName, string testName, string data, TestError error)
    {
        summary = new TestSummary(passed, actions, suiteName, testName, data, error);
    }

    public bool IsActivated()
    {
        return visualStream != null;
    }

    public string GetDownloadFile()
    {
        if (IsActivated())
        {
            visualStream.Flush();
            Thread.Sleep(1000);

            fileVisualStream.Close();
            visualStream.Close();

            Thread.Sleep(1000);
        }

        visualStream = null;
        fileVisualStream = null;
        AmfSerializer = null;

        return AtsvFilePath;
    }

    /*internal void Create(string actionType, int actionLine, string actionScript, long timeLine, string channelName, double[] channelBound, bool sync, bool stop){
        Create(actionType, actionLine, actionScript, timeLine, channelName, channelBound, sync, stop, new DriverInfo());
    }*/

    internal void Create(string actionType, int actionLine, string actionScript, long timeLine, string channelName, double[] channelBound, bool sync, bool stop, DriverInfo driverInfo)
    {
        if(driverInfo.sessionId != API_SESSION)
        {
            if (channelBound[2] > 1.0 && channelBound[3] > 1.0)
            {
                if (driverInfo.headless)
                {
                    //image via browser
                    currentAction.AddImage(GetScreenshotFromWebDriver(driverInfo.screenshotUrl));
                }
                else
                {
                    currentAction.AddImage(this, channelBound, false);
                }
            }
        }

        Flush();

        if (driverInfo.headless){
            if (sync){
                currentAction = new VisualActionSync(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, VisualAction.WEBP_TYPE, GetScreenshotFromWebDriver(driverInfo.screenshotUrl), null, null, 0.0F, 0.0F);
            }
            else 
            {
                if (driverInfo.sessionId == API_SESSION)
                {
                    currentAction = new VisualAction(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, API_TYPE, null);
                }
                else
                {
                    currentAction = new VisualAction(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, VisualAction.WEBP_TYPE, GetScreenshotFromWebDriver(driverInfo.screenshotUrl));
                }
            }
        }
        else if (sync) {
            currentAction = new VisualActionSync(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, imageType, ScreenCapture(channelBound), null, null, 0.0F, 0.0F);
        }
        else {
            currentAction = new VisualAction(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, imageType, ScreenCapture(channelBound), null, null, 0.0F, 0.0F);
         }
    }

    internal void CreateMobile(string actionType, int actionLine, string actionScript, long timeLine, string channelName, double[] channelBound, string url, bool sync, bool stop)
    {
        if (sync)
        {
            currentAction.AddImage(this, url, channelBound, false);
        }

        Flush();

        if (sync)
        {
            currentAction = new VisualActionSync(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, imageType, ScreenCapture(channelBound, url), null, null, 0.0F, 0.0F);
        }
        else
        {
            currentAction = new VisualAction(stop, actionType, actionLine, actionScript, timeLine, channelName, channelBound, imageType, ScreenCapture(channelBound, VisualAction.GetScreenshotImage(url)), null, null, 0.0F, 0.0F);
        }
    }

    internal void AddImage(String screenShotUrl)
    {
       currentAction.AddImage(GetScreenshotFromWebDriver(screenShotUrl));
    }

    internal void AddImage(double[] screenRect, bool isRef)
    {
        currentAction.AddImage(this, screenRect, isRef);
    }

    internal void AddImage(string url, double[] screenRect, bool isRef)
    {
        currentAction.AddImage(this, url, screenRect, isRef);
    }

    internal void AddRecord(string path)
    {
        currentAction.AddRecord(path);
    }

    internal void AddValue(string v)
    {
        currentAction.setValue(v);
    }

    internal void AddData(string v1, string v2)
    {
        AddValue(v1);
        currentAction.setData(v2);
    }

    internal void Status(int error, long duration)
    {
        currentAction.setDuration(duration);
        currentAction.setError(error);
    }

    internal void AddElement(double[] bound, long duration, int found, string criterias, string tag)
    {
        currentAction.setElement(new VisualElement(tag, criterias, found, bound, duration));
    }

    internal void AddPosition(string hpos, string hposValue, string vpos, string vposValue)
    {
        currentAction.UpdateElementPosition(hpos, hposValue, vpos, vposValue);
    }

    internal void Flush()
    {
        frameIndex = currentAction.setIndex(frameIndex);

        if (IsActivated())
        {
            currentAction.WriteAction(AmfSerializer, visualStream);
        }
    }

    private static byte[] GetResourceImage(string fileName)
    {
        using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("windowsdriver.resources." + fileName))
        {
            var data = new byte[stream.Length];
            stream.Read(data, 0, data.Length);

            return data;
        }
    }

    public static byte[] GetWebpImage(Bitmap bitmap)
    {
        using (var outStream = new System.IO.MemoryStream())
        {
            try
            {
                Imazen.WebP.SimpleEncoder imazen = new Imazen.WebP.SimpleEncoder();
                imazen.Encode(bitmap, outStream, imageQuality);

                return outStream.ToArray();
            }
            catch (System.Runtime.InteropServices.ExternalException ex)
            {
                Console.WriteLine($"GDI+ error: {ex.Message}");
                throw new Exception("Failed during WebP encoding: ", ex);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Argument error : {ex.Message}");
                throw new Exception("Invalid parameters for WebP encoding: ", ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unexpected error: {ex.Message}");
                throw;
            }
            finally
            {
                bitmap.Dispose();
                outStream.Close();
            }
        }
    }

    public static byte[] ScreenCapturePng(DesktopManager desk, List<string> screenData)
    {
        Rectangle rect = desk.DesktopRect;
        if (screenData.Count > 1 && screenData[1].Length > 3)
        {
            int[] numbers = screenData[1].Split(',').Select(Int32.Parse).ToArray();
            rect = new Rectangle(numbers[0], numbers[1], numbers[2], numbers[3]);
        }
        return ScreenCapturePng(rect.X, rect.Y, rect.Width, rect.Height);
    }

    public static byte[] ScreenCapturePng(int x, int y, int w, int h)
    {
        Bitmap bmp = ScreenCapture(x, y, w, h);

        EncoderParameters encoderParameters = new EncoderParameters(1);
        encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

        MemoryStream imageStream;
        using (imageStream = new MemoryStream())
        {
            bmp.Save(imageStream, GetEncoder(ImageFormat.Png), encoderParameters);
        }
        bmp.Dispose();
        return imageStream.ToArray();
    }

    private static ImageCodecInfo GetEncoder(ImageFormat format)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
        foreach (ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }

    public static Bitmap GetScreenshotFromWebDriver(string screenShotUrl)
    {
        if (!string.IsNullOrEmpty(screenShotUrl))
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    string json = client.DownloadString(screenShotUrl);
                    JObject jsonObj = JObject.Parse(json);
                    string base64Image = jsonObj["value"].ToString();

                    byte[] image = Convert.FromBase64String(base64Image);
                    return new Bitmap(new MemoryStream(image));
                }
                catch (Exception ex)
                {
                    AtsLogger.WriteError("Erreur lors de la récupération du screenshot -> {0}", ex.Message);
                }
            }
        }
        return null;
    }

    public void Reset()
    {
        frameIndex = 0;
        visualStream?.Dispose();
        visualStream = null;
        fileVisualStream?.Dispose();
        fileVisualStream = null;
        currentAction = new VisualActionNull();
        //animationEncoderParameters?.Dispose();
        imageType = "";
        AmfSerializer=null;
        AtsvFilePath = "";
        currentPid = -1;
    }
    
}