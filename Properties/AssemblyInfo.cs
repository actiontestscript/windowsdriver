﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using static DesktopDriver;

// Les informations générales relatives à un assembly dépendent de
// l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
// associées à un assembly.

[assembly: AssemblyTitle("windowsdriver")]
[assembly: AssemblyDescription("ATS system driver for Windows")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ActionTestScript")]
[assembly: AssemblyProduct("windowsdriver")]
[assembly: AssemblyCopyright("Copyright ActionTestScript 2019")]
[assembly: AssemblyTrademark("ActionTestScript")]
[assembly: AssemblyCulture("")]

// L'affectation de la valeur false à ComVisible rend les types invisibles dans cet assembly
// aux composants COM. Si vous devez accéder à un type dans cet assembly à partir de
// COM, affectez la valeur true à l'attribut ComVisible sur ce type.
[assembly: ComVisible(false)]

// Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
[assembly: Guid("74e056e4-4ed4-4554-ae2f-7764fc60e48a")]

// Les informations de version pour un assembly avec ATS se composent des quatre valeurs suivantes :
//      Version principale
//      Version secondaire
//      Version tertiaire
//      Numéro de build
// Please use only 3 groups of numbers, the last group will be set by CI

[assembly: AssemblyVersion("1.9.0")]
[assembly: AssemblySapVersion("1.0.6")]
