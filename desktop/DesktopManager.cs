﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;
using FlaUI.Core.Definitions;
using FlaUI.Core.Tools;
using FlaUI.Core.WindowsAPI;
using FlaUI.UIA3;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using windowsdriver.desktop;
using windowsdriver.items;
using windowsdriver.utils;
using static FlaUI.Core.FrameworkAutomationElementBase;

namespace windowsdriver
{
    public class DesktopManager
    {
        public readonly DesktopElement DesktopElement;

        private readonly Dictionary<int, WindowHandle> windows = new Dictionary<int, WindowHandle>();
        private readonly List<PopupHandle> popups = new List<PopupHandle>();

        private readonly UIA3Automation uia3;
        private readonly AutomationElement desktop;

        public readonly int DesktopWidth;
        public readonly int DesktopHeight;

        public readonly Rectangle DesktopRect;

        private readonly AndCondition TopModalCondition;
        private readonly PropertyCondition WindowCondition;
        private readonly PropertyCondition MenuItemCondition;
        private readonly PropertyCondition MenuCondition;

        public DesktopManager()
        {
            DesktopRect = SystemInformation.VirtualScreen;
            DesktopWidth = DesktopRect.Width;
            DesktopHeight = DesktopRect.Height;

            uia3 = new UIA3Automation();
            uia3.ConnectionTimeout = new TimeSpan(0, 0, 1);
            uia3.TransactionTimeout = new TimeSpan(0, 0, 1);

            desktop = uia3.GetDesktop();

            //--------------------------------------------------
            // init conditions
            //--------------------------------------------------

            TopModalCondition = desktop.ConditionFactory.ByControlType(ControlType.Pane).And(desktop.ConditionFactory.ByClassName("Alternate Modal Top Most"));
            // ModalCondition = desktop.ConditionFactory.ByControlType(ControlType.Window).And(desktop.ConditionFactory.ByLocalizedControlType("boîte de dialogue"));
            // ModalConditionWpf = desktop.ConditionFactory.ByControlType(ControlType.Window).And(desktop.ConditionFactory.ByLocalizedControlType("fenêtre"));
            WindowCondition = desktop.ConditionFactory.ByControlType(ControlType.Window);
            MenuItemCondition = desktop.ConditionFactory.ByControlType(ControlType.MenuItem);
            MenuCondition = desktop.ConditionFactory.ByControlType(ControlType.Menu);

            //--------------------------------------------------
            // wait desktop completly ready
            //--------------------------------------------------

            desktop.FindFirstChild();
            
            //--------------------------------------------------

            AutomationElement[] desktopChildren = AtsElement.FindAllChildren(desktop);
            DesktopElement = new DesktopElement(desktop, DesktopRect);
            for (int i = 0; i < desktopChildren.Length; i++)
            {
                AddDesktopChildren(desktopChildren[i]);
            }

            desktop.RegisterStructureChangedEvent(TreeScope.Children, (element, type, arg) => StructureChanged(type, element));
        }

        public void Dispose()
        {
            uia3.Dispose();
        }

        public int GetDevicesCount()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE (PNPClass = 'Camera')");
            return searcher.Get().Count;
        }

        public void HandleRemove(int hdl, bool reduce)
        {
            if (windows.TryGetValue(hdl, out WindowHandle whdl))
            {
                if (reduce)
                {
                    whdl.Reduce();
                }

                whdl.Dispose();
                windows.Remove(hdl);
            }
        }

        public void PopupRemove(PopupHandle pop)
        {
            popups.Remove(pop);
            pop.Dispose();
        }

        public bool AddPopup(int pid, AutomationElement element)
        {
            try
            {
                element.Properties.ClassName.TryGetValue(out string className);
                return AddPopup(className, pid, element);
            }
            catch { }
            return false;
        }

        public bool AddPopup(string className, int pid, AutomationElement element)
        {
            if (className == "Popup" || className == "Pane" || className == "Menu" || className == "#32768")
            {
                AddPopupList(pid, element);
                return true;
            }
            else
            {
                element.Properties.ControlType.TryGetValue(out ControlType type);
                if (ControlType.Menu == type)
                {
                    AddPopupList(pid, element);
                    return true;
                }
            }
            return false;
        }

        private void AddPopupList(int pid, AutomationElement element)
        {
            PopupHandle pop = new PopupHandle(this, pid, element);
            popups.Add(pop);
        }

        private void StructureChanged(StructureChangeType type, AutomationElement element)
        {
            if (type.Equals(StructureChangeType.ChildAdded) || type.Equals(StructureChangeType.ChildrenBulkAdded))
            {
                AddDesktopChildren(element);
            }
        }

        private void AddDesktopChildren(AutomationElement element)
        {
            try
            {
                element.Properties.ClassName.TryGetValue(out string className);

                if (className != "SysShadow" && className != "WindowsFormsSapFocus" && className != "Intermediate D3D Window")
                {
                    int pid = element.Properties.ProcessId;

                    if (!AddPopup(className, pid, element) && element.Properties.NativeWindowHandle.IsSupported)
                    {
                        IntPtr handle = element.Properties.NativeWindowHandle;
                        if (handle != IntPtr.Zero)
                        {
                            int hdl = handle.ToInt32();
                            if (windows.ContainsKey(hdl))
                            {
                                if (windows.TryGetValue(hdl, out WindowHandle wh))
                                {
                                    wh.Update(element);
                                    return;
                                }
                            }
                            else
                            {
                                WindowHandle wh = new WindowHandle(this, pid, hdl, element);
                                windows.Add(hdl, wh);
                            }
                        }
                    }
                }
            }
            catch { }
        }

        internal DesktopWindow GetAppMainWindow(Process proc)
        {
            List<DesktopWindow> wins = GetMainWindowsByProc(proc);
            if (wins.Count > 0)
            {
                return wins[wins.Count - 1];
            }

            return null;
        }

        internal DesktopWindow GetConsoleWindow(Process proc)
        {
            int pid = proc.Id;

            foreach (AutomationElement w in GetDesktopWindows())
            {
                try // Windows 10 behaviour
                {
                    if (w.Properties.ProcessId == pid)
                    {
                        return new DesktopWindow(w);
                    }
                }
                catch { }
            }

            foreach (AutomationElement w in GetDesktopWindows())
            {
                try // Windows 11 behaviour
                {
                    if (w.Name.ToLower().Contains("\\system32\\cmd.exe"))
                    {
                        AutomationElement[] childs = w.FindAllChildren();
                        foreach (AutomationElement c in childs)
                        {
                            if (c.Properties.ProcessId == pid)
                            {
                                return new DesktopWindow(w);
                            }
                        }
                    }
                }
                catch { }
            }

            return null;
        }
        internal DesktopWindow GetPowerShellWindow(Process proc)
        {
            int pid = proc.Id;

            foreach (AutomationElement w in GetDesktopWindows())
            {
                try // Windows 11 behaviour
                {
                    if ("Windows PowerShell".Equals(w.Name))
                    {
                        AutomationElement[] childs = w.FindAllChildren();
                        foreach (AutomationElement c in childs)
                        {
                            if (c.Properties.ProcessId == pid)
                            {
                                return new DesktopWindow(w);
                            }
                        }
                    }
                }
                catch { }
            }

            return null;
        }


        //-------------------------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------------------

        public AtsElement GetElementFromPoint(Point pt)
        {
            AutomationElement elem = uia3.FromPoint(pt);
            if (elem != null)
            {
                return new AtsElement(elem);
            }
            return null;
        }

        //-------------------------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------------------

        private List<WindowHandle> GetHandlesFromPids(List<int> pids)
        {
            int documentIndex = 0;

            List<int> listToDelete = new List<int>();
            List<WindowHandle> list = new List<WindowHandle>();

            foreach (KeyValuePair<int, WindowHandle> entry in windows)
            {
                if (entry.Value.HasNoParent())
                {
                    listToDelete.Add(entry.Key);
                }
                else if (entry.Value.CheckPid(pids))
                {
                    if (entry.Value.IsDocument())
                    {
                        list.Insert(documentIndex, entry.Value);
                        documentIndex++;
                    }
                    else
                    {
                        list.Add(entry.Value);
                    }
                }
            }

            listToDelete.ForEach(k => windows.Remove(k));

            return list;
        }

        public DesktopWindow GetWindowIndexByPid(int pid, int index)
        {
            if (index > -1)
            {
                if (pid == 0)
                {
                    return new DesktopWindow(windows[index].Win);
                }
                else
                {
                    List<int> pids = new List<int>();
                    _ = new ProcessTree(Process.GetProcessById(pid), pids);

                    List<WindowHandle> h = GetHandlesFromPids(pids);

                    if (index < h.Count)
                    {
                        return new DesktopWindow(h[index].Win);
                    }
                }
            }
            return null;
        }

        public DesktopWindow GetJxWindowPid(string title)
        {

            AutomationElement[] wins = desktop.FindAllChildren();

            //-------------------------------------------------------------------------------------------------
            // Try to find JXBrowser main window
            //-------------------------------------------------------------------------------------------------

            for (int i = 0; i < wins.Length; i++)
            {
                AutomationElement win = wins[i];
                if (win.Properties.AutomationId.IsSupported && win.Properties.Name.IsSupported
                    && win.AutomationId.Equals("JavaFX1")){

                    string windowName = win.Properties.Name;
                    if (windowName.ToLower().Contains(title.ToLower()))
                    {
                        return new DesktopWindow(win);
                    }
                }
            }
            return null;
        }

        public DesktopWindow GetWindowByTitle(string title)
        {
            Focus();

            int maxTry = 5;
            DesktopWindow window = null;

            while (window == null && maxTry > 0)
            {
                Thread.Sleep(50);
                window = TryToGetWindowByName(title);
                maxTry--;
            }

            if (window != null)
            {
                return window;
            }

            return new DesktopWindow(this);
        }

        private DesktopWindow TryToGetWindowByName(string name)
        {
            uia3.ConnectionTimeout = new TimeSpan(0, 0, 50);
            AutomationElement[] wins = desktop.FindAllChildren();
            uia3.ConnectionTimeout = new TimeSpan(0, 0, 1);

            //-------------------------------------------------------------------------------------------------
            // try to find standard window
            //-------------------------------------------------------------------------------------------------

            for (int i = 0; i < wins.Length; i++)
            {
                AutomationElement window = wins[i];
                string windowName = null;
                try
                {
                    windowName = window.Properties.Name;
                    if (windowName != null && windowName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return new DesktopWindow(window);
                    }
                }
                catch { }
            }

            //-------------------------------------------------------------------------------------------------
            // second chance to find the window
            //-------------------------------------------------------------------------------------------------

            for (int i = 0; i < wins.Length; i++)
            {
                AutomationElement[] windowChildren = wins[i].FindAllChildren();
                DesktopWindow found = null;

                Parallel.For(0, windowChildren.Length, (x, loopState) =>
                {
                    AutomationElement windowChild = windowChildren[x];
                    string winName = null;
                    try
                    {
                        winName = windowChild.Name;
                    }
                    catch { }

                    if (winName != null && winName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        found = new DesktopWindow(windowChild);
                        loopState.Break();
                    }
                });

                if (found != null)
                {
                    return found;
                }

                /*for (int j = 0; j < windowChildren.Length; j++)
                {
                    AutomationElement windowChild = windowChildren[j];
                    string winName = null;
                    try
                    {
                        winName = windowChild.Name;
                    }
                    catch { }

                    if (winName != null && winName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return new DesktopWindow(windowChild, this);
                    }
                }*/
            }

            return null;
        }

        public DesktopWindow GetWindowByName(string title)
        {
            DesktopWindow window = GetWindowByTitle(title);
            window.UpdateProcessData();

            return window;
        }

        public DesktopWindow GetWindowByHandle(string handle)
        {
            DesktopWindow window = GetWindowByHandle(Int32.Parse(handle));
            window.UpdateProcessData();

            return window;
        }

        #region GetWindowByHandle
        /// <summary>
        /// Search window by handle
        /// </summary>
        /// <param name="handle">window handle</param>
        /// <returns>DesktopWindow</returns>
        internal DesktopWindow GetWindowByHandle(int handle)
        {
            if (handle < 1) return DesktopElement;

            DesktopWindow win = null;

            if (windows.ContainsKey(handle)) //Found in windows list
            {
                if (windows.TryGetValue(handle, out WindowHandle wh))
                {
                    win = new DesktopWindow(wh.Win);
                }
            }
            else //Search on desktop
            {
                win = GetWindowFromHandle(handle);
                if (win == null) win = GetWindowByHandleInList(handle);
            }
            return win;
        }

        /// <summary>
        /// Search in priority with FromHandle methode (faster)
        /// </summary>
        /// <param name="handle">window handle</param>
        /// <returns>DesktopWindow</returns>
        private DesktopWindow GetWindowFromHandle(int handle)
        {
            Window window = null;
            try
            {
                window = uia3.FromHandle((IntPtr)handle).AsWindow();
            }
            catch
            {
                // Raise error ?
                return null;
            }

            DesktopWindow win = new DesktopWindow(window);
            try
            {
                windows.Add(win.Handle, new WindowHandle(this, win.Pid, win.Handle, win.Element));
            }
            catch {}
            return win;
        }

        /// <summary>
        /// Search in windows list in priority (slower)
        /// </summary>
        /// <param name="handle">window handle</param>
        /// <returns>DesktopWindow</returns>
        private DesktopWindow GetWindowByHandleInList(int handle)
        {
            int maxTry = 20;
            while(!windows.ContainsKey(handle) && maxTry > 0)
            {
                Thread.Sleep(500);
                maxTry--;
            }

            if (maxTry == 0)
            {
                // Shouldn't we just look for the handle rather than updating the whole list?
                foreach (AutomationElement child in GetDesktopWindows())
                {
                    DesktopWindow win = new DesktopWindow(child);
                    if (!windows.ContainsKey(win.Handle))
                    {
                        try
                        {
                            windows.Add(win.Handle, new WindowHandle(this, win.Pid, win.Handle, win.Element));
                        }
                        catch { }
                    }
                }
            }

            if (windows.TryGetValue(handle, out WindowHandle wh))
            {
                return new DesktopWindow(wh.Win);
            }

            return null;
        }
        #endregion

        public DesktopWindow GetFirstWindowsByPid(int pid)
        {
            AutomationElement[] wins = desktop.FindAllChildren();
            for (int i = 0; i < wins.Length; i++)
            {
                AutomationElement window = wins[i];
                if (window.Properties.ProcessId == pid)
                {
                    return new DesktopWindow(window);
                }
            }
            return null;
        }

        public List<DesktopWindow> GetOrderedWindowsByPid(int pid)
        {
            List<DesktopWindow> windowsList = new List<DesktopWindow>();

            try
            {

                List<WindowHandle> hs = GetHandlesFromPids(new List<int> { pid });

                if (hs.Count > 0)
                {
                    for (int i = 0; i < hs.Count; i++)
                    {
                        windowsList.Add(new DesktopWindow(hs[i].Win));
                    }

                    //hs.ForEach(h => windowsList.Add(new DesktopWindow(h.Win)));
                }
                else
                {
                    foreach (AutomationElement child in GetDesktopChildrenById(pid))
                    {
                        DesktopWindow win = new DesktopWindow(child);
                        if (!windows.ContainsKey(win.Handle))
                        {
                            try
                            {
                                windows.Add(win.Handle, new WindowHandle(this, pid, win.Handle, win.Element));
                            }
                            catch { }
                        }
                        windowsList.Add(win);
                    }
                }
            }catch { };

            return windowsList;
        }

        private AutomationElement[] GetDesktopWindows()
        {

            int maxTry = 40;

            while (maxTry > 0)
            {
                try
                {
                    return desktop.FindAllChildren(WindowCondition);
                }
                catch (COMException)
                {
                    Thread.Sleep(250);
                }
                maxTry--;
            }

            return new AutomationElement[0]; ;
        }

        private AutomationElement[] GetDesktopChildrenById(int pid)
        {
            PropertyCondition pidCondition = desktop.ConditionFactory.ByProcessId(pid);

            int maxTry = 40;

            while (maxTry > 0)
            {
                try
                {
                    return desktop.FindAllChildren(pidCondition);
                }
                catch (COMException)
                {
                    Thread.Sleep(250);
                }
                maxTry--;
            }

            return new AutomationElement[0]; ;
        }

        private List<AutomationElement> GetWindowsByProc(Process proc)
        {
            List<int> pids = new List<int>();
            _ = new ProcessTree(proc, pids);

            List<AutomationElement> list = new List<AutomationElement>();
            foreach (AutomationElement w in GetDesktopWindows())
            {
                if (pids.IndexOf(w.Properties.ProcessId) > -1 && !string.IsNullOrEmpty(w.Properties.Name))
                {
                    list.Add(w);
                }
            }
            return list;
        }

        public List<DesktopWindow> GetMainWindowsByProc(Process proc)
        {
            int maxTry = 40;
            List<AutomationElement> desktopWins = GetWindowsByProc(proc);
            while (maxTry > 0 && desktopWins.Count == 0)
            {
                Thread.Sleep(500);
                desktopWins = GetWindowsByProc(proc);
                maxTry--;
            }

            string appName = "_";
            try
            {
                appName = Regex.Replace(proc.MainModule.ModuleName, ".exe$", "");
            }
            catch { }

            List<DesktopWindow> wins = new List<DesktopWindow>();
            foreach (AutomationElement w in desktopWins)
            {
                wins.Add(new DesktopWindow(w, appName));
            }

            return wins;
        }

        public DesktopWindow GetWindowByProcess(Process proc)
        {
            int maxTry = 20;
            DesktopWindow window = null;
            int mainWindowHandle = proc.MainWindowHandle.ToInt32();
            if (mainWindowHandle > 0)
            {
                while (window == null && maxTry > 0)
                {
                    System.Threading.Thread.Sleep(500);
                    window = GetWindowByHandle(mainWindowHandle);
                    maxTry--;
                }
            }
            else
            {
                window = GetWindowByTitle(proc.ProcessName);
            }

            if (window == null)
            {
                throw new Exception("No window found");
            }

            window.UpdateApplicationData(proc);
            return window;
        }

        public AutomationElement GetFirstModalPane()
        {
            AutomationElement modalPane = desktop.FindFirstChild(TopModalCondition);
            int maxTry = 20;
            while (modalPane == null && maxTry > 0)
            {
                modalPane = desktop.FindFirstChild(TopModalCondition);
                Thread.Sleep(200);
                maxTry--;
            }
            return modalPane;
        }

        #region GetMsgBoxs
        /// <summary>
        /// Find msgbox
        /// </summary>
        /// <param name="pId">process id</param>
        /// <returns>List of modal windows with 2 or more buttons</returns>
        public AtsElement[] GetMsgBoxs(int pId = -1)
        {
            List<AtsElement> atsElements = new List<AtsElement>();

            //In first case we searching msgbox in the main windows of the canal
            if (pId > -1)
            {
                DesktopWindow window = GetFirstWindowsByPid((int)pId);
                List<AutomationElement> elements = window.Element.FindAll(TreeScope.Descendants, WindowCondition).ToList();
                elements.Add(window.Element);

                foreach (AutomationElement element in elements)
                {
                    atsElements = GetMsgBoxs(WindowCondition, element);
                    if (atsElements.Count > 0) break;
                }
            }

            //In second case we searching msgbox on the desktop
            if (atsElements.Count == 0)
            {
                AutomationElement[] elements = desktop.FindAll(TreeScope.Children, WindowCondition);
                foreach (AutomationElement element in elements)
                {
                    atsElements = GetMsgBoxs(WindowCondition, element);
                    if (atsElements.Count > 0) break;
                }
            }
            return atsElements.ToArray();
        }

        /// <summary>
        /// Search msgbox in window baseSearch
        /// </summary>
        /// <param name="modalCondition">condition to find msgbox</param>
        /// <param name="baseSearch">object where we search msgbox</param>
        /// <returns>List of msgbox</returns>
        private List<AtsElement> GetMsgBoxs(ConditionBase modalCondition, AutomationElement baseSearch)
        {
            List<AtsElement> atsElements = new List<AtsElement>();
            AutomationElement[] elements = baseSearch.FindAll(TreeScope.Children, modalCondition);
            foreach (AutomationElement element in elements)
            {
                IFrameworkPatterns patterns = element.Patterns;
                if ((patterns.Window.IsSupported &&
                    patterns.Window.Pattern.IsModal &&
                    !patterns.Window.Pattern.CanMaximize &&
                    !patterns.Window.Pattern.CanMinimize))
                {
                    AtsElement atsElement = new AtsElement(element);
                    List<AtsElement> lstChildren = new List<AtsElement>();
                    //List of buttons
                    AutomationElement[] tabAE = element.FindAllChildren(cf => cf.ByControlType(ControlType.Button));
                    if (tabAE.Count() > 0)
                    {
                        foreach (AutomationElement child in tabAE)
                        {
                            AtsElement elem = new AtsElement(child);
                            string text = string.Empty;
                            try
                            {
                                text = child.Name;
                            }
                            catch { }
                            if (!string.Empty.Equals(text))
                            {
                                DesktopData data = new DesktopData("Text", text);
                                elem.Attributes = (new List<DesktopData>() { data }).ToArray();

                                lstChildren.Add(elem);
                            }
                        }
                    }
                    atsElement.Children = lstChildren.ToArray();
                    atsElements.Add(atsElement);
                }
            }
            return atsElements;
        }
        #endregion

        public HashSet<AtsElement> GetElements(string tag, string[] attributes)
        {
            return DesktopElement.GetElements(false, tag, attributes, desktop, this);
        }

        public Stack<AutomationElement> GetPopupDescendants(int pid)
        {
            Stack<AutomationElement> list = new Stack<AutomationElement>();
            popups.FindAll(p => p.Pid == pid).ForEach(e =>
            {
                foreach (AutomationElement elem in e.GetElements())
                {
                    list.Push(elem);
                }
            });
            return list;
        }

        public AutomationElement[] GetPopupListElements()
        {
            List<AutomationElement> list = new List<AutomationElement>();
            foreach (PopupHandle pop in popups)
            {
                list.AddRange(pop.GetElements());
            }
            return list.ToArray();
        }

        public HashSet<AtsElement> GetPopupsElements()
        {
            HashSet<AtsElement> list = new HashSet<AtsElement>();
            foreach (PopupHandle pop in popups)
            {
                foreach (AutomationElement e in pop.GetElements())
                {
                    list.Add(new AtsElement(e));
                }
            }
            return list;
        }

        public AtsElement[] GetPopupListAtsElement()
        {
            List<AtsElement> list = new List<AtsElement>();
            foreach (PopupHandle pop in popups)
            {
                list.AddRange(pop.GetListItems());
            }
            return list.ToArray();
        }

        public AutomationElement[] GetContextMenu()
        {
            if (desktop.FindFirst(TreeScope.Children, MenuCondition) != null)
            {
                return desktop.FindAll(TreeScope.Subtree, MenuCondition);
            }
            return null;
        }

        public AutomationElement[] GetContextMenuItems()
        {
            AutomationElement context = desktop.FindFirst(TreeScope.Children, MenuCondition);
            if (context != null)
            {
                return context.FindAll(TreeScope.Subtree, MenuItemCondition);
            }
            return new AutomationElement[0];
        }

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------

        public static bool IsDesktopComponent(string className)
        {
            return className.StartsWith("Shell_")
                    || className.StartsWith("TaskList")
                    || className == "SysListView32"
                    || className == "Progman"
                    || className == "NotifyIconOverflowWindow"
                    || className == "Windows.UI.Core.CoreWindow";
        }

        internal void Focus()
        {
            try
            {
                AutomationElement traybar = desktop.FindFirstChild(desktop.ConditionFactory.ByClassName("Shell_TrayWnd"));
                AutomationElement startButton = traybar.FindFirstChild(traybar.ConditionFactory.ByClassName("Start"));
                if (startButton == null)
                {
                    traybar.Focus();
                    AutomationElement taskbar = traybar.FindFirstDescendant(traybar.ConditionFactory.ByAutomationId("TaskbarFrame"));
                    if (taskbar != null)
                    {
                        startButton = taskbar.FindFirstDescendant(taskbar.ConditionFactory.ByAutomationId("StartButton"));
                    }
                }
                startButton.Focus();
            }
            catch { }
        }
    }
}
