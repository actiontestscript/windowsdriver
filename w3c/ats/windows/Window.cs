﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System;
using System.Collections.Generic;
using System.Net;

namespace windowsdriver.w3c.ats.windows
{
    public abstract class Window : JsonResponse
    {

        //-------------------------------------------------
        // com.ats.executor.ActionStatus -> status code
        //-------------------------------------------------
        public const int UNREACHABLE_GOTO_URL = -11;
        public const int WINDOW_NOT_FOUND = -14;
        public const int CLOSE_WINDOW = -19;
        //-------------------------------------------------
        public enum WindowType
        {
            Title = 0,
            Handle = 1,
            List = 2,
            Move = 3,
            Resize = 4,
            ToFront = 5,
            Switch = 6,
            Close = 7,
            Url = 8,
            Keys = 9,
            State = 10,
            CloseWindow = 11,
            CloseModalWindows = 12
        };

        public static readonly Dictionary<string, int> windowTypeDict = new Dictionary<string, int>()
    {
        {"title", 0},
        {"handle", 1},
        {"list", 2},
        {"move", 3},
        {"resize", 4},
        {"tofront", 5},
        {"switch", 6},
        {"close", 7},
        {"url", 8},
        {"keys", 9},
        {"state", 10},
        {"closewindow", 11},
        {"closemodalwindows", 12}
    };

        public static WindowType? getEnumWindowType(string type)
        {
            type = type.ToLower();
            WindowType? windowType = null;
            foreach (var pair in windowTypeDict)
            {
                if (string.Equals(pair.Key, type, StringComparison.OrdinalIgnoreCase))
                {
                    windowType = (WindowType)pair.Value;
                    break;
                }
            }
            return windowType;
        }

        public ActionKeyboard keyboard;
        public DesktopManager desktop;
        public DesktopWindow window;
        public VisualRecorder recorder;
        protected DesktopResponse desktopResponse;

        public Window(HttpListenerContext context, ActionKeyboard keyboard, DesktopManager desktop, VisualRecorder recorder) : base(context)
        {
            this.keyboard = keyboard;
            this.desktop = desktop;
            this.recorder = recorder;
            this.desktopResponse = new DesktopResponse(context);
        }

        public virtual Boolean Execute()
        {
            return base.SetJsonValue();
        }
        protected void SendDesktopResponse(DesktopWindow[] value)
        {
            desktopResponse.SetWindows(value);
            desktopResponse.Send(desktopResponse);
        }

        protected void SendDesktopResponse(DesktopWindow value)
        {
            desktopResponse.SetWindow(value);
            desktopResponse.Send(desktopResponse);
        }

        protected void SendDesktopResponse()
        {
            desktopResponse.Send(desktopResponse);
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }

}
