﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using System.Net;

namespace windowsdriver.w3c.session
{
    public class SessionWindow : SessionW3c
    {
        protected SessionW3CType? sessionW3CType;
        protected HttpListenerContext context;
        public SessionWindow(SessionW3CType? sessionW3cType, HttpListenerContext context) : base(context) {
            this.sessionW3CType = sessionW3cType;
            this.context = context;
        }

        public override bool Execute()
        {
            var windowFactory = new windowsdriver.w3c.session.window.WindowFactory();
            var windowCommand = windowFactory.Create(sessionW3CType, context);
            if (windowCommand != null)
            {
                return windowCommand.Execute();
            }
            return false;
        }
    }
}