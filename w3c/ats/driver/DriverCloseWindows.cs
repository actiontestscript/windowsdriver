﻿/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;
using windowsdriver.utils;

namespace windowsdriver.w3c.ats.driver
{
     public class DriverCloseWindowsData
    {
        public int processId = 0;
        public int handle = -1;
    }
    public class DriverCloseWindows : Driver
    {
        public DriverCloseWindows(HttpListenerContext context, Capabilities capabilities, VisualRecorder recorder, DesktopManager desktop) : base(context, capabilities, recorder, desktop) { }
        private DriverCloseWindowsData datas = new DriverCloseWindowsData();
        public override bool Execute()
        {
            if (base.Execute())
            {
                datas = JsonConvert.DeserializeObject<DriverCloseWindowsData>(jsonValue);
                if( datas.handle > 0)
                {
                    DesktopWindow winapp = desktop.GetWindowByHandle(datas.handle);
                    if (winapp != null)
                    {
                        winapp.Close();
                        Thread.Sleep(500);
                    }
                }

                if ( datas.processId > 0 )
                {
                    Process proc = Process.GetProcessById(datas.processId);
                    if (proc != null && proc.MainModule != null && (proc.MainModule.ModuleName == "WindowsTerminal.exe" || proc.MainModule.ModuleName == "explorer.exe"))
                    {
                        Console.WriteLine("WindowsTerminal or Explorer detected ...");
                    }
                    else
                    {
                        try
                        {
                            List<DesktopWindow> wins = desktop.GetOrderedWindowsByPid(datas.processId);
                            while (wins.Count > 0)
                            {
                                wins[0].Close();
                                wins.RemoveAt(0);
                            }
                        }
                        catch { }

                        try
                        {
                            if (proc != null)
                            {
                                int maxTry = 30;
                                while (maxTry > 0 && !proc.HasExited)
                                {
                                    Thread.Sleep(100);
                                    maxTry--;
                                }

                                if (proc != null)
                                {
                                    proc.Close();
                                    Thread.Sleep(500);

                                    proc = Process.GetProcessById(datas.processId);
                                    proc?.Kill();
                                }
                            }
                        }
                        catch { }
                    }
                }
                else
                {
                    desktopResponse.SetError(errorCode, "pid must be greater than 0");
                    Dispose();
                    SendDesktopResponse();
                    return false;
                }
                Dispose();
                SendDesktopResponse();
                return true;
            }
            Dispose();
            SendDesktopResponse();
            return false;
        }
    }
}